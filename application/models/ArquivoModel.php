<?php
defined('BASEPATH') or exit('No direct script access allowed');

class ArquivoModel extends CI_Model
{
    public function recebe_arquivo()
    {
        $nome = $this->input->post('nome');
        $nome_arquivo = $this->nome_arquivo($nome);
        $config['upload_path'] = './upload/';
        $config['allowed_types'] = 'jpg';
        $config['max_size'] = 2048;
        $config['file_name'] = $nome_arquivo . '.jpg';
        $this->load->library('upload', $config);

        if ($this->upload->do_upload('arquivo')) {
            echo 'Arquivo enviado com sucesso';
        } 
    }

    private function nome_arquivo($nome)
    {
        $n = strtolower($nome);
        $v = explode(" ", $n);
        $s = implode("_", $v);
        return $s . '_' . date('dmyhis');
    }

    public function lista_arquivos()
    {
        $html = '<table>';
        $dir = './upload/';
        $a = scandir($dir, 1);

        foreach ($a as $key => $arq){
            $html .= '<tr>';
            $html .= "<td>$arq<td>";
            $html .= "<td>$key<td>";
            $html .= '</tr>';
        }
        return '</table>';
    }
}
