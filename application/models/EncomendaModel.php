<?php
defined('BASEPATH') or exit('No direct script access allowed');
include APPPATH . 'libraries/cadastro/Validacao_encomenda.php';
include APPPATH . 'libraries/cadastro/insere_encomenda.php';

class EncomendaModel extends CI_Model
{
    public function listaAdm()
    {
        $sql = "SELECT * FROM contato";
        $rs = $this->db->query($sql);
        $m = $rs->result();
        $html = '';

        foreach ($m as $row) {
            $html .= '<tr>';
            $html .= "<td>$row->nome</td>";
            $html .= "<td>$row->email</td>";
            $html .= "<td>$row->telefone</td>";
            $html .= "<td>$row->mensagem</td>";
            $html .= "<td>$row->cep</td>";
            $html .= "<td>$row->rua</td>";
            $html .= "<td>$row->bairro</td>";
            $html .= "<td>$row->numero</td>";
            $html .= "<td>$row->cidade</td>";
            $html .= "<td>$row->uf</td>";
            $html .= '<td>' . $this->get_action_buttons($row->id) . '</td>';
            $html .= "</tr>";
        }

        return $html;
    }

    private function get_action_buttons($id)
    {
        $html = '<a href="' . base_url('index.php/encomenda/remover/' . $id) . '">
        <i class="fas fa-times mr-3 text-danger"></i>';

        return $html;
    }

    public function remover($id)
    {
        $this->db->delete('contato', "id = $id");
        redirect('Encomenda/listar');
    }

        public function salvar()
    {
        //evita que o formuário grave registros vazios
        if (sizeof($_POST) == 0) return;
        $encomenda = new Validacao_encomenda();
        $encomenda->validacao();

        if ($this->form_validation->run()) {
            $data = new insere_encomenda();
            $v = $encomenda->getData();
            $data->gravar($v);
            echo "<script>
            alert('Enviado!');
            window.location.href = '" . base_url() . "index.php/encomenda';
            </script>";
        } else {
            echo "<script>
            alert('Erro ao enviar!');
            window.location.href = '" . base_url() . "index.php/encomenda';
            </script>";
        }
    }

}
