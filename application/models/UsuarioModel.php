<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include APPPATH.'libraries/component/UserCard.php';

class UsuarioModel extends CI_Model{

    public function criar(){
        //evita que o formuário grave registros vazios
        if(! sizeof($_POST)) return;
        $nome = $this->input->post('first_name');
        $sobrenome = $this->input->post('last_name');
        $senha = $this->input->post('password');
        $email = $this->input->post('email');
        $telefone = $this->input->post('phone');
        

        $sql = "INSERT INTO users(first_name, last_name, password, email, phone,) VALUES ('$nome', '$sobrenome', '$email', '$telefone', ' $senha')";
        $this->db->query($sql);
    }

    public function listar(){
        $sql = "SELECT * FROM users";
        $rs = $this->db->query($sql);
        $m = $rs->result();
        $html = '';

        foreach ($m as $row) {
            $html .= '<tr>';
            $html .= '<td>'.$row->first_name.'</td>';
            $html .= "<td>$row->last_name</td>";
            $html .= "<td>$row->password</td>";
            $html .= "<td>$row->email</td>";
            $html .= "<td>$row->phone</td>";
            $html .= '<td>'.$this->get_action_buttons($row->id).'</td>';
            $html .= "</tr>";
        }

        return $html;
    }


    private function get_action_buttons($id){
        $html = '<a href="'.base_url('usuario/atualizar/'.$id).'">
        <i class="fas fa-highlighter mr-3 text-primary"></i>';

        $html .= '<a href="'.base_url('usuario/remover/'.$id).'">
        <i class="fas fa-times mr-3 text-danger"></i>';

        $html .= '<a href="'.base_url('usuario/detalhe/'.$id).'">
        <i class="fas fa-search text-muted"></i>';

        return $html;
     }

    public function detalhe($id){
        $sql = "SELECT * FROM `users` WHERE id = $id";
        $rs = $this->db->query($sql);
        return $rs->result_array()[0];
    }

    public function atualizar($id){
        if(! sizeof($_POST)) return;

        $data = $this->input->post();

        // pesquise ActiveRecord do codeIgniter
        $this->db->update('users', $data, "id = $id");
        redirect('users');
    }

    public function remover($id){
        $this->db->delete('users', "id = $id");
    }

    public function lista_usuario(){
        $html ='';

        // dados para inicialização dos objetos
        $rs = $this->db->get('users');
        $m = $rs->result_array();

        foreach ($m as $usuario) { 
          $card = new UserCard($usuario);
          $html .= $card->getHTML();
        }

        return $html;

    }
}