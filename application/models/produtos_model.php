<?php
defined('BASEPATH') or exit('No direct script access allowed');
include APPPATH . 'libraries/cadastro/Validacao_produto.php';
include APPPATH . 'libraries/cadastro/insere_produto.php';

class Produtos_model extends CI_Model
{
    public function buscaTodos()
    {
        $sql = "SELECT * FROM produtos";
        $rs = $this->db->query($sql);
        $m = $rs->result();
        $html = '';

        foreach ($m as $row) {
            $html .= '<tr class="table-purple">';
            $html .= "<td>$row->nome_produto</td>";
            $html .= "<td>$row->tamanho</td>";
            $html .= moeda("<td>R$$row->preco</td>");
            $html .= "</tr>";
        }

        return $html;
    }

    public function buscaAdm()
    {
        $sql = "SELECT * FROM produtos";
        $rs = $this->db->query($sql);
        $m = $rs->result();
        $html = '';

        foreach ($m as $row) {
            $html .= '<tr class="table-purple">';
            $html .= "<td>$row->nome_produto</td>";
            $html .= "<td>$row->tamanho</td>";
            $html .= moeda("<td>R$$row->preco</td>");
            $html .= '<td>' . $this->get_action_buttons($row->id) . '</td>';
            $html .= "</tr>";
        }

        return $html;
    }

    private function get_action_buttons($id)
    {
        $html = '<a href="' . base_url('index.php/produto/atualizar/' . $id) . '">
        <i class="fas fa-highlighter mr-3 text-primary"></i>';

        $html .= '<a href="' .base_url('index.php/produto/remover/'.$id). '">
        <i class="fas fa-times mr-3 text-danger"></i>';

        return $html;
    }

    public function atualizar($id)
    {
        if (!sizeof($_POST)) return;
        $data = $this->input->post();
        // pesquise ActiveRecord do codeIgniter
        $this->db->update('produtos', $data, "id = $id");
        redirect('produto/lista');
    }

    public function remover($id)
    {
        // $this->db->where('id', $id);
        // $this->db->remover('produtos');
        // return true;
        $this->db->delete('produtos', "id = $id");
        redirect('produto/lista');
    }

    public function salva()
    {
        //evita que o formuário grave registros vazios
        if (sizeof($_POST) == 0) return;
        $produto = new Validacao_produto();
        $produto->validate();

        if ($this->form_validation->run()) {
            $data = new insere_produto();
            $v = $produto->getData();
            $data->gravar($v);
            echo "<script>
            alert('Produto cadastrado com sucesso!');
            window.location.href = '" . base_url() . "index.php/produto/lista';
            </script>";
        } else {
            echo "<script>
            alert('Formulário inválido!');
            window.location.href = '" . base_url() . "index.php/produto';
            </script>";
        }
    }
    public function criar(){
        //evita que o formuário grave registros vazios
        if(! sizeof($_POST)) return;
        $nome_produto = $this->input->post('nome_produto');
        $tamanho = $this->input->post('tamanho');
        $preco = $this->input->post('preco');

        $sql = "INSERT INTO produtos(nome_produto, tamanho, preco,) VALUES ('$nome_produto', '$tamanho', ' $preco')";
        $this->db->query($sql);
    }
}
