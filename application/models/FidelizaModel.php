<?php
defined('BASEPATH') or exit('No direct script access allowed');
include APPPATH . 'libraries/fidelizacao/Valida_CPF.php';
include APPPATH . 'libraries/fidelizacao/FD.php';

class FidelizaModel extends CI_Model
{
    public function salva()
    {
        //evita que o formuário grave registros vazios
        if (sizeof($_POST) == 0) return;
        $fidelizacao = new Valida_CPF();
        $fidelizacao->valida();

        if ($this->form_validation->run()) {
            $data = new FD();
            $v = $fidelizacao->getData();
            $data->gravar($v);
            echo "<script>
            alert('Enviado!');
            window.location.href = '" . base_url() . "index.php/fidelizacao';
            </script>";
        } else {
            echo "<script>
            alert('Erro ao enviar!');
            window.location.href = '" . base_url() . "index.php/fidelizacao';
            </script>";
        }
    }

    private function get_action_buttons($id)
    {
        $html = '<a href="' . base_url('index.php/Fidelizacao/remover/' . $id) . '">
        <i class="fas fa-times mr-3 text-danger"></i>';

        return $html;
    }

    public function remover($id)
    {
        $this->db->delete('fidelizacao', "id = $id");
        redirect('Fidelizacao/listar');
    }

    public function lista_fidel()
    {
        $html = '';

        $sql = "SELECT * FROM fidelizacao";
        $rs = $this->db->query($sql);
        $m = $rs->result();
        $html = '';

        foreach ($m as $row) {
            $html .= '<tr>';
            $html .= '<td>' . $row->nome . '</td>';
            $html .= "<td>$row->email</td>";
            $html .= "<td>$row->CPF</td>";
            $html .= '<td>' . $this->get_action_buttons($row->id) . '</td>';
            $html .= "</tr>";
        }

        return $html;
    }

    /**
     * Verifica se o CPF informado é valido
     * @param     string
     * @return     bool
     */
    function valid_cpf($cpf)
    {
        // Verifiva se o número digitado contém todos os digitos
        $cpf = str_pad(preg_replace('/[^0-9]/', '', $cpf), 11, '0', STR_PAD_LEFT);

        // Verifica se nenhuma das sequências abaixo foi digitada, caso seja, retorna falso
        if (
            strlen($cpf) != 11 ||
            $cpf == '00000000000' ||
            $cpf == '11111111111' ||
            $cpf == '22222222222' ||
            $cpf == '33333333333' ||
            $cpf == '44444444444' ||
            $cpf == '55555555555' ||
            $cpf == '66666666666' ||
            $cpf == '77777777777' ||
            $cpf == '88888888888' ||
            $cpf == '99999999999'
        ) {
            return FALSE;
        } else {
            // Calcula os números para verificar se o CPF é verdadeiro
            for ($t = 9; $t < 11; $t++) {
                for ($d = 0, $c = 0; $c < $t; $c++) {
                    $d += $cpf{
                        $c} * (($t + 1) - $c);
                }

                $d = ((10 * $d) % 11) % 10;
                if ($cpf{
                    $c} != $d) {
                    return FALSE;
                }
            }
            return TRUE;
        }
    }
}
