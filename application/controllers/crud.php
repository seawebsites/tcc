<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Crud extends CI_Controller
{

    public function index()
    {
        $this->load->view('common/header');
        $this->load->view('common/navbar_crud');
        $this->load->view('usuario/cadastro_usuario');

        $this->load->model('UsuarioModel');
        $v['lista_usuario'] = $this->UsuarioModel->lista_usuario();
        $this->load->view('usuario/lista', $v);
        $this->load->view('common/footer');
    }
    public function interface(){
        $this->load->view('common/header');
        $this->load->view('common/navbar2');
        $this->load->view('usuario/usuario');
        $this->load->view('common/footer');
    }
    

}
