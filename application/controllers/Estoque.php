<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Estoque extends CI_Controller{

    public function index(){
        $this->load->view('common/header');       
        $this->load->view('common/navbar');
        $this->load->view('index.php');
        $this->load->view('common/footer');
    }

    public function entrada(){
        $this->load->view('common/header');
        $this->load->view('common/navbar');
        $this->load->view('common/footer');
    }

    public function saida(){
        echo 'exibe o formulário para solicitação de produtos de estoque';
    }

    public function balanco(){
        echo 'Exibe a página que apresenta o relatório de quantidades de produtos em estoque';
    }

    public function busca($param){
        echo 'Página para verificação sobre a existência de algum produto. <br><br>';
        echo "Produto procurado: $param";
    }
}