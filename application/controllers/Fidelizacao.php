<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Fidelizacao extends CI_Controller
{

    public function index()
    {
        $this->load->view('common/header');
        $this->load->view('common/navbar2');

        $this->load->model('FidelizaModel');
        $this->FidelizaModel->salva();

        $this->load->view('Fidelizacao/form_cadastrofidel.php');
        $this->load->view('common/footer');
    }
    
    public function remover($id)
    {
        $this->load->model('FidelizaModel');
        $this->FidelizaModel->remover($id);
        redirect('form_cadastrofidel');
    }
    public function listar()
    {
        // verifica se o usuário está logado
        // e se ele é administrador do sistema
        // exibe mensagem de alerta para usuarios comuns
        if (!$this->ion_auth->logged_in()) {
            echo "<script>
            alert('Acesso negado!');
            window.location.href = '" . base_url() . "index.php/inicio';
            </script>";
        }
        $this->load->view('common/header');
        $this->load->view('common/navbar_crud');

        $this->load->model('FidelizaModel');
        $dados['table'] = $this->FidelizaModel->lista_fidel();

        $this->load->view('Fidelizacao/lista_fidel', $dados);
        $this->load->view('common/footer');
    }
}
