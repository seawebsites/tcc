<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Contato extends CI_Controller
{
    public function index()
    {
        $this->load->view('common/header');
        $this->load->view('common/navbar2');
        $this->load->view('contato/Localizacao.php');
        $this->load->view('common/footer');
    }
}
