<?php 
defined('BASEPATH') or exit('No direct script access allowed');
 
	class Carrinho extends CI_Controller {

		public function index()
    {	   	   
        $this->load->view('common/header');
		$this->load->view('common/navbar2');
		
		$this->load->model('CarrinhoModel');
		$this->load->helper('funcoes');
		$this->CarrinhoModel->salva();

        $this->load->view('Car/carrinho.php');
		
		
		$dados['produto'] = $this->CarrinhoModel->retorna_produtos();
		$this->load->view('Paginas/doces', $dados);
		   

        $this->load->view('common/footer');
    }
			
		public function listar(){

			$this->load->view('common/navbar2');
			$this->load->view('common/header');
			

			$this->load->view("Car/carrinho");
			
			$this->load->view('common/footer');

		}
		public function atualizar($id){

			$this->load->view('common/header');
			$this->load->view('common/navbar2');
			$this->load->model('CarrinhoModel');
		
			$this->CarrinhoModel->atualizar($id);
			$dados['table'] = $this->CarrinhoModel->buscaAdm($id);
			$this->load->view('Car/carrinho.php', $dados);
			
		
			$this->load->view('common/footer');

			
			//recebo todo o conteúdo postado do formulário e no loop abaixo recupero o que preciso
			$conteudo_postado = $this->input->post();
			
			foreach($conteudo_postado as $conteudo) {
				
				$dados[] = array(
				
					"rowid" => $conteudo['rowid'],
					"qty" => $conteudo['qty']
				
				);
					
			}
			//com os dados já preparados, basta dar um update no carrinho
			$this->cart->update($dados);
			
			redirect(base_url('index.php/carrinho/listar'));
			
		}
		public function inserir(){
			
			
			$id = $this->input->post("id");
			$produto = $this->input->post("nome_produto");
			$tamanho = $this->input->post("tamanho");
			$preco = $this->input->post("preco");
			
			//se o usuário não informar a quantidade do produto, então, coloco 1
			if (empty($tamanho)) 
				$tamanho = 1;
			
			//A biblioteca do carrinho de compras já está carregada lá pelo autoload, então não preciso chamá-la aqui novamente.
			
			//Esta linha serve para permitir que produtos com acentuação no nome sejam aceitos.
			$this->cart->product_name_rules = "'\d\D'";
			 
			$data = array(
               'id'      => $id,
               'qty'     => $tamanho,
               'price'   => $preco,
               'name'    => $produto
            );
 
			
			
			
			if ($this->cart->insert($data)) {
				redirect(base_url('index.php/carrinho/listar'));
			} else {
				echo "ERRO. Não foi possível inserir. <pre>";
				print_r($data);
				echo "</pre>";				
			}
			
		}
		//função que limpa o conteúdo do carrinho de compras.
		public function limpar(){
			
			$this->load->model('Produtos_model');

			$this->cart->destroy();
			redirect(base_url('index.php/carrinho/listar'));
			
		}
		
	}