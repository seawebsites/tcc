<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Encomenda extends CI_Controller
{

    public function index()
    {
        $this->load->view('common/header');
        $this->load->view('common/navbar2');

        $this->load->view('formulario/form_encomenda');

        $this->load->model('EncomendaModel');
        $this->EncomendaModel->salvar();

        $this->load->view('common/footer');
    }

    public function remover($id)
    {
        $this->load->model('EncomendaModel');
        if($this->EncomendaModel->remover($id)){
            echo "<script>
            alert('Mensagem apagada!');
            window.location.href = '" . base_url() . "index.php/formulario/pedidos';
            </script>";
        }
    }

    public function listar()
    {
        // verifica se o usuário está logado
        // e se ele é administrador do sistema
        // exibe mensagem de alerta para usuarios comuns
        if (!$this->ion_auth->logged_in()) {
            echo "<script>
            alert('Acesso negado!');
            window.location.href = '" . base_url() . "index.php/inicio';
            </script>";
        }
        $this->load->view('common/header');
        $this->load->view('common/navbar_crud');

        $this->load->model('EncomendaModel');
        $dados['table'] = $this->EncomendaModel->listaAdm();

        $this->load->view('formulario/pedidos', $dados);
        $this->load->view('includes/footer');
    }

    public function arquivo()
    {
        $this->load->model('ArquivoModel', 'model');
        $this->model->recebe_arquivo();

        $data['lista_arquivos'] = $this->model->lista_arquivos();
        $html = $this->load->view('formulario/form_upload_arquivo', $data, true);
        $this->show($html);
    }

}
