<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Produto extends CI_Controller
{

    /**
     * Insere as informações no banco de dados.
     */
    public function index()
    {
        // verifica se o usuário está logado
        // e se ele é administrador do sistema
        // exibe mensagem de alerta para usuarios comuns
        if (!$this->ion_auth->logged_in()) {
            echo "<script>
            alert('Acesso negado!');
            window.location.href = '" . base_url() . "index.php/inicio';
            </script>";
        }

        $this->load->view('common/header');
        $this->load->view('common/navbar_crud');
        $this->load->view('Paginas/formulario.php');

        $this->load->model('Produtos_model');
        $this->Produtos_model->salva();

        $this->load->view('includes/footer');
    }
    public function lista()
    {
        // verifica se o usuário está logado
        // e se ele é administrador do sistema
        // exibe mensagem de alerta para usuarios comuns
        if (!$this->ion_auth->logged_in()) {
            echo "<script>
            alert('Acesso negado!');
            window.location.href = '" . base_url() . "index.php/inicio';
            </script>";
        }

        $this->load->view('common/header');
        $this->load->view('common/navbar_crud');

        $this->load->model('Produtos_model');
        $dados['tabela'] = $this->Produtos_model->buscaAdm();

        $this->load->view('Paginas/lista.php', $dados);
        $this->load->view('includes/footer');
    }

    public function atualizar($id)
    {
        $this->load->view('common/header');
        $this->load->view('common/navbar_crud');
        $this->load->model('Produtos_model');

        $this->Produtos_model->atualizar($id);
        $data['table'] = $this->Produtos_model->buscaAdm($id);
        $this->load->view('Paginas/formulario.php', $data);

        $this->load->view('common/footer');
    }

    public function remover($id)
    {
        $this->load->model('Produtos_model');
        $this->Produtos_model->remover($id);
    }

    public function bolos()
    {
        $this->load->view('common/header');
        $this->load->view('common/navbar2');

        $this->load->model('Produtos_model');
        $dados['table'] = $this->Produtos_model->buscaTodos();

        $this->load->view('Paginas/bolos.php', $dados);
        $this->load->view('common/footer');
    }

    public function tortas()
    {
        $this->load->view('common/header');
        $this->load->view('common/navbar2');
        $this->load->view('Paginas/tortas.php');
        $this->load->view('common/footer');
    }

    public function bebidas()
    {
        $this->load->view('common/header');
        $this->load->view('common/navbar2');
        $this->load->view('Paginas/bebidas.php');
        $this->load->view('common/footer');
    }
    public function doces()
    {
        $this->load->view('common/header');
        $this->load->view('common/navbar2');
        $this->load->view('Paginas/doces.php');
        $this->load->view('common/footer');
    }
    
}
