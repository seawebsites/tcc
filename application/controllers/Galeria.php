<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Galeria extends CI_Controller{

    public function index(){
        $this->load->view('common/header');       
        $this->load->view('common/navbar2');
        $this->load->view('Galeria/Fotos.php');
        $this->load->view('common/footer');
    }
}