<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Entrar extends CI_Controller
{
    public function index(){
        $this->load->view('common/header');
        $this->load->view('auth/login.php');
        $this->load->view('includes/footer');
    }
}