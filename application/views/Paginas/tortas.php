<br />
<section class="text-center my-5">
<br>
<div id="portfolio-heading" class="portfolio-heading section-heading">Tortas</div>
<br>
<div class="conatiner">
<div class="card-group">
<div class="col-lg-3 col-md-6 mb-md-0 mb-4">
  <div class="card mb-4">
    <div class="view overlay">
    <div class="card card-cascade narrower card-ecommerce">
        <div class="view view-cascade overlay">
        <img class="card-img-top" src="<?= base_url('assets/mdb/img/Imagens/tortas.jpg') ?>" alt="Card image cap">
      <a href="#!">
        <div class="mask rgba-white-slight"></div>
      </a>
            <div class="mask rgba-white-slight"></div>
          </a>
        </div>
        <div class="card-body card-body-cascade text-center">
          <a href="" class="grey-text">
          <h5>Torta de Frango</h5>
          </a>
         
        
          <div class="card-footer px-1">
            <span class="float-left font-weight-bold">
              <strong>Valor: de 25,00 R$ a 30,00 </strong>
            </span>
          </div>
        </div>
      </div>
</div>
    </div>
  </div>
  <br /><br />
  <div class="col-lg-3 col-md-6 mb-md-0 mb-4">
  <div class="card mb-4">
    <div class="view overlay">
    <div class="card card-cascade narrower card-ecommerce">
        <div class="view view-cascade overlay">
        <img class="card-img-top" src="<?= base_url('assets/mdb/img/Imagens/torta.jpg') ?>" alt="Card image cap">
        
        <div class="mask rgba-white-slight"></div>
    
        </div>
        <div class="card-body card-body-cascade text-center">
          <div class="grey-text">
          <h5>Torta de Frango com Catupiry	</h5>
</div>
          
          
<div class="card-footer px-1">
            <span class="float-left font-weight-bold">
              <strong>Valor: de 25,00 R$ a 30,00</strong>
            </span>
          </div>
        </div>
      </div>
      
    </div>
</div>
  </div>
<div class="col-lg-3 col-md-6 mb-md-0 mb-4">
  <div class="card mb-4">
    <div class="view overlay">
    <div class="card card-cascade narrower card-ecommerce">
        <div class="view view-cascade overlay">
        <img class="card-img-top" src="<?= base_url('assets/mdb/img/Imagens/torta_salgada.jpg') ?>" alt="Card image cap">
          <a href="#!">
        <div class="mask rgba-white-slight"></div>
      </a>
            <div class="mask rgba-white-slight"></div>
          </a>
        </div>
        <div class="card-body card-body-cascade text-center">
          <div class="grey-text">
          <h5>Torta de Palmito</h5>
</div>
          
         
         
          <div class="card-footer px-1">
            <span class="float-left font-weight-bold">
              <strong>Valor: de 25,00 R$ a 30,00</strong>
            </span>
          </div>
        </div>
      </div>
      
      
    </div>
    </div>
</div>
<div class="col-lg-3 col-md-6 mb-md-0 mb-4">
  <div class="card mb-4">
    <div class="view overlay">
    <div class="card card-cascade narrower card-ecommerce">
        <div class="view view-cascade overlay">
        <img class="card-img-top" src="<?= base_url('assets/mdb/img/Imagens/cuscuz.jpg') ?>" alt="Card image cap">
      <a href="#!">
        <div class="mask rgba-white-slight"></div>
      </a>
            <div class="mask rgba-white-slight"></div>
          </a>
        </div>
        <div class="card-body card-body-cascade text-center">
          <div class="grey-text">
          <h5>Cuzcus Salgado</h5>
</div>
          
         
<div class="card-footer px-1">
            <span class="float-left font-weight-bold">
              <strong>Valor: 25,00 R$</strong>
            </span>
          </div>
        </div>
      </div>
</div>
    </div>
  </div>
  <div class="col-lg-3 col-md-6 mb-md-0 mb-4">
  <div class="card mb-4">
    <div class="view overlay">
    <div class="card card-cascade narrower card-ecommerce">
        <div class="view view-cascade overlay">
        <img class="card-img-top" src="<?= base_url('assets/mdb/img/Imagens/torta-limao.jpg') ?>" alt="Card image cap">
      <a href="#!">
        <div class="mask rgba-white-slight"></div>
      </a>
            <div class="mask rgba-white-slight"></div>
          </a>
        </div>
        <div class="card-body card-body-cascade text-center">
          <div class="grey-text">
          <h5>Torta de Limão</h5>
</div>
          
         
<div class="card-footer px-1">
            <span class="float-left font-weight-bold">
            <strong><a href="https://api.whatsapp.com/send?phone=5511988588235&text=Olá Gostaria de saber o preço da torta de limão">Valor a consultar</a></strong>
            </span>
          </div>
        </div>
      </div>
</div>
    </div>
  </div>
  <div class="col-lg-3 col-md-6 mb-md-0 mb-4">
  <div class="card mb-4">
    <div class="view overlay">
    <div class="card card-cascade narrower card-ecommerce">
        <div class="view view-cascade overlay">
        <img class="card-img-top" src="<?= base_url('assets/mdb/img/Imagens/torta_doce.jpg') ?>" alt="Card image cap">
      <a href="#!">
        <div class="mask rgba-white-slight"></div>
      </a>
            <div class="mask rgba-white-slight"></div>
          </a>
        </div>
        <div class="card-body card-body-cascade text-center">
          <div class="grey-text">
          <h5>Torta de Morango</h5>
</div>
          
         
<div class="card-footer px-1">
            <span class="float-left font-weight-bold">
              <strong><a href="https://api.whatsapp.com/send?phone=5511988588235&text=Olá Gostaria de saber o preço da torta doce">Valor a consultar</a></strong>
            </span>
          </div>
        </div>
      </div>
</div>
    </div>
  </div>
  <br /><br />
</div>
</section>
</div>
