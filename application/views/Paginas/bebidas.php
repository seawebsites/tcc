<br>
<div id="portfolio-heading" class="portfolio-heading section-heading">Tabela de Preços</div>
<br>

<div class="text-center">
    <h3>Bebidas</h3>
</div>
<div class="container">
    <div class="row">
        <div class="d-flex justify-content-around">
            <table class="table table-striped w-auto">
                <thead>
                    <tr>
                        <th>Bebidas</th>
                        <th>Preços</th>
                    </tr>
                </thead>
                <tr class="table-purple">
                    <td>Coca-Cola 350ml</td>
                    <td>R$5,00</td>
                </tr>
                <tr>
                    <td>Coca-Cola 220ml</td>
                    <td>R$5,00</td>
                </tr>
                <tr class="table-purple">
                    <td>Coca-Cola Zero 220ml</td>
                    <td>R$3,00</td>
                </tr>
                <tr>
                    <td>Água sem Gás 1,5L</td>
                    <td>R$5,00</td>
                </tr>
                <tr class="table-purple">
                    <td>Água sem Gás 500ml</td>
                    <td>R$2,00</td>
                </tr>
                
            </table>

            <table class="table table-striped w-auto">
                <thead>
                    <tr>
                        <th>Bebidas</th>
                        <th>Preços</th>
                    </tr>
                </thead>
                <tr class="table-purple">
                    <td>Água com Gás 500ml</td>
                    <td>R$3,00</td>
                </tr>
                <tr>
                    <td>Chpa Mate Pêssego </td>
                    <td>R$5,00</td>
                </tr>
                <tr class="table-purple">
                    <td>Sprite 220ml</td>
                    <td>R$3,00</td>
                </tr>
                <tr>
                    <td>Coca-Cola 2L</td>
                    <td>R$10,00</td>
                </tr>
                <tr class="table-purple">
                    <td>Enérgetico Monster</td>
                    <td>R$9,00</td>
                </tr>
            </table>
        </div>
    </div>
</div>
<br>
