<br>
<div id="portfolio-heading" class="portfolio-heading section-heading">Produtos</div>
<br>
<div class="container">
  <div class="row">
  <a role="button" href="<?= base_url('index.php/produto') ?>" class="btn btn-default">Novo Produto</a>
    <div class="d-flex justify-content-md-between">
      <table class="table table-striped w-auto">
        <thead>
          <tr>
            <th>Produtos</th>
            <th>Tamanhos</th>
            <th>Preços</th>
            <th>Funções</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <?= $tabela ?>
          </tr>
        </tbody>
      </table>
    </div>
  </div>
</div>
<br>