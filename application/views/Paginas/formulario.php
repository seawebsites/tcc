<br>
<div class="container col-md-4">
    <form method="POST" class="text-center">
        <p class="h4 mb-4">Cadastro de Produto</p>
        <input required type="text" value="<?= isset($produto['nome_produto']) ?  $produto['nome'] : ''?>" id="nome_produto" name="nome_produto" class="form-control mb-4" placeholder="Nome do Produto">
        <input type="text" value="<?= isset($produto['tamanho']) ?  $produto['tamanho'] : '' ?>" id="tamanho" name="tamanho" class="form-control mb-4" placeholder="Tamanho do Produto">
        <input required value="<?= isset($produto['preco']) ?  $produto['preco'] : '' ?>" type="text" id="preco" name="preco" class="form-control mb-4" placeholder="Preço do Produto">

        <button class="btn btn-default my-4 btn-block" type="submit">Cadastrar</button>
    </form>
</div>