<br>
<div id="portfolio-heading" class="portfolio-heading section-heading">Tabela de Preços</div>
<br>
<div class="container">
  <div class="row">
    <div class="d-flex justify-content-around">
      <table class="table table-striped w-auto">
        <thead>
          <tr>
            <th>Produtos</th>
            <th>Tamanhos</th>
            <th>Preços</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <?= $table ?>
          </tr>
        </tbody>
      </table>
    </div>
  </div>
</div>
<br>
<div class="text-center">
    <h3>Acresimos</h3>
</div>
<div class="container">
    <div class="row">
        <div class="d-flex justify-content-around">
            <table class="table table-striped w-auto">
                <thead>
                    <tr>
                        <th>Nome</th>
                        <th>Preços</th>
                    </tr>
                </thead>
                <tr class="table-purple">
                    <td>Bem Casado</td>
                    <td><a href="https://api.whatsapp.com/send?phone=5511988588235&text=Olá Gostaria de saber o preço do bem casado">R$ consultar valor</a></td>
                </tr>
                <tr>
                    <td>Bolo Abacaxi</td>
                    <td><a href="https://api.whatsapp.com/send?phone=5511988588235&text=Olá Gostaria de saber o preço do bolo abacaxi">R$ consultar valor</a></td>
                </tr>
                <tr class="table-purple">
                    <td>Manjar</td>
                    <td><a href="https://api.whatsapp.com/send?phone=5511988588235&text=Olá Gostaria de saber o preço do manjar">R$ consultar valor</a></td>
                </tr>
                <tr>
                    <td>Bolo Neked Cake - Kg</td>
                    <td>R$45,00</td>
                </tr>
                <tr class="table-purple">
                    <td>Cobertura Extra</td>
                    <td>R$6,00</td>
                </tr>
                
            </table>
        </div>
    </div>
</div>
<br>

<br />
<section class="text-center my-5">
<br>
<div id="portfolio-heading" class="portfolio-heading section-heading">Bolos Caseiros</div>
<br>
<div class="conatiner">
<div class="card-group">
<div class="col-lg-3 col-md-6 mb-md-0 mb-4">
  <div class="card mb-4">
    <div class="view overlay">
    <div class="card card-cascade narrower card-ecommerce">
        <div class="view view-cascade overlay">
        <img class="card-img-top" src="<?= base_url('assets/mdb/img/Imagens/bolo_goiabada.jpg') ?>" alt="Card image cap">
        <div class="mask rgba-white-slight"></div>
            <div class="mask rgba-white-slight"></div>
          </a>
        </div>
        <div class="card-body card-body-cascade text-center">
          <a href="" class="grey-text">
            <h5>Bolo fubá com goiabada </h5>
          </a>
          <div class="card-footer px-1">
            <span class="float-left font-weight-bold">
              <strong>Pequeno: 10,00$</strong>
              <br />
              <strong>Grande: 18,00$</strong>
            </span>
            
          </div>
        </div>
      </div>
</div>
    </div>
  </div>
  <br /><br />
  <div class="col-lg-3 col-md-6 mb-md-0 mb-4">
  <div class="card mb-4">
    <div class="view overlay">
    <div class="card card-cascade narrower card-ecommerce">
        <div class="view view-cascade overlay">
        <img class="card-img-top" src="<?= base_url('assets/mdb/img/Imagens/bolo-fuba.jpg') ?>" alt="Card image cap">
        <div class="mask rgba-white-slight"></div>
        </div>
        <div class="card-body card-body-cascade text-center">
          <a href="" class="grey-text">
          <h5>Bolo fubá cremoso </h5>
          </a>
          <div class="card-footer px-1">
            <span class="float-left font-weight-bold">
            <strong>Pequeno: 10,00$</strong>
              <br />
              <strong>Grande: 18,00$</strong>
              <a href="" class="grey-text">
            <h5>Bolo fubá</h5>
          </a>
            <strong>Pequeno: 10,00$</strong>
              <br />
              <strong>Grande: 16,00$</strong>
              <br />
              <a href="" class="grey-text">
            <h5>Bolo fubá com erva-doce</h5>
          </a>
            <strong>Pequeno: 10,00$</strong>
              <br />
              <strong>Grande: 16,00$</strong>
            </span>
            
          </div>
        </div>
      </div>
      
    </div>
</div>
  </div>
<div class="col-lg-3 col-md-6 mb-md-0 mb-4">
  <div class="card mb-4">
    <div class="view overlay">
    <div class="card card-cascade narrower card-ecommerce">
        <div class="view view-cascade overlay">
        <img class="card-img-top" src="<?= base_url('assets/mdb/img/Imagens/bolo-coco.jpg') ?>" alt="Card image cap">
        <div class="mask rgba-white-slight"></div>
            <div class="mask rgba-white-slight"></div>
          </a>
        </div>
        <div class="card-body card-body-cascade text-center">
          <a href="" class="grey-text">
            <h5>Bolo de coco</h5>
          <div class="card-footer px-1">
            <span class="float-left font-weight-bold">
            <strong>Pequeno: 10,00$</strong>
              <br />
              <strong>Grande: 16,00$</strong>
            </span>
           
          </div>
        </div>
      </div>
      
      
    </div>
    </div>
</div>
<div class="col-lg-3 col-md-6 mb-md-0 mb-4">
  <div class="card mb-4">
    <div class="view overlay">
    <div class="card card-cascade narrower card-ecommerce">
        <div class="view view-cascade overlay">
        <img class="card-img-top" src="<?= base_url('assets/mdb/img/Imagens/bolo-pudim.jpg') ?>" alt="Card image cap">
        <div class="mask rgba-white-slight"></div>
            <div class="mask rgba-white-slight"></div>
          </a>
        </div>
        <div class="card-body card-body-cascade text-center">
          <a href="" class="grey-text">
            <h5>Pudim</h5>
            <div class="card-footer px-1">
            <span class="float-left font-weight-bold">
            <strong><a href="https://api.whatsapp.com/send?phone=5511988588235&text=Olá Gostaria de saber o preço do pudim">Preço a consultar</a></strong>
            </span>
          </div>
        </div>
      </div>
      
      
    </div>
    </div>
</div>
<div class="col-lg-3 col-md-6 mb-md-0 mb-4">
  <div class="card mb-4">
    <div class="view overlay">
    <div class="card card-cascade narrower card-ecommerce">
        <div class="view view-cascade overlay">
        <img class="card-img-top" src="<?= base_url('assets/mdb/img/Imagens/bolo-limao.jpg') ?>" alt="Card image cap">
        <div class="mask rgba-white-slight"></div>
            <div class="mask rgba-white-slight"></div>
          </a>
        </div>
        <div class="card-body card-body-cascade text-center">
          <a href="" class="grey-text">
            <h5>Bolo de limão</h5>
          </a>
          <div class="card-footer px-1">
            <span class="float-left font-weight-bold">
            <a href="" class="grey-text">
            <h5>Sem Cobertura</h5>
          </a>
            <strong>Pequeno: 10,00$</strong>
              <br />
              <strong>Grande: 16,00$</strong>
              <br />
              <a href="" class="grey-text">
            <h5>Com Cobertura</h5>
          </a>
            <strong>Pequeno: 12,00$</strong>
              <br />
              <strong>Grande: 22,00$</strong>
            </span>
            
          </div>
        </div>
      </div>
</div>
    </div>
  </div>
  <br /><br />
  <div class="col-lg-3 col-md-6 mb-md-0 mb-4">
  <div class="card mb-4">
    <div class="view overlay">
    <div class="card card-cascade narrower card-ecommerce">
        <div class="view view-cascade overlay">
        <img class="card-img-top" src="<?= base_url('assets/mdb/img/Imagens/bolo-cenoura.jpg') ?>" alt="Card image cap">
        <div class="mask rgba-white-slight"></div>
        </div>
        <div class="card-body card-body-cascade text-center">
          <a href="" class="grey-text">
            <h5>Bolo cenoura com cobertura de chocolate</h5>
          </a>
          <div class="card-footer px-1">
            <span class="float-left font-weight-bold">
            <strong>Pequeno: 12,00$</strong>
              <br />
              <strong>Grande: 22,00$</strong>
              <a href="" class="grey-text">
            <h5>Bolo de cenoura sem cobertura</h5>
          </a>
            <strong>Pequeno: 10,00$</strong>
              <br />
              <strong>Grande: 16,00$</strong>
            </span>
            
          </div>
        </div>
      </div>
      
    </div>
</div>
  </div>
<div class="col-lg-3 col-md-6 mb-md-0 mb-4">
  <div class="card mb-4">
    <div class="view overlay">
    <div class="card card-cascade narrower card-ecommerce">
        <div class="view view-cascade overlay">
        <img class="card-img-top" src="<?= base_url('assets/mdb/img/Imagens/banana-canela.jpg') ?>" alt="Card image cap">
        <div class="mask rgba-white-slight"></div>
            <div class="mask rgba-white-slight"></div>
          </a>
        </div>
        <div class="card-body card-body-cascade text-center">
          <a href="" class="grey-text">
            <h5>Bolo de banana com canela</h5>
          </a>
          <div class="card-footer px-1">
            <span class="float-left font-weight-bold">
          </a>
            <strong>Pequeno: 10,00$</strong>
              <br />
              <strong>Grande: 18,00$</strong>
            </span>
           
          </div>
        </div>
      </div>
      
      
    </div>
    </div>
</div>
<div class="col-lg-3 col-md-6 mb-md-0 mb-4">
  <div class="card mb-4">
    <div class="view overlay">
    <div class="card card-cascade narrower card-ecommerce">
        <div class="view view-cascade overlay">
        <img class="card-img-top" src="<?= base_url('assets/mdb/img/Imagens/bolo-geleia.jpg') ?>" alt="Card image cap">
        <div class="mask rgba-white-slight"></div>
            <div class="mask rgba-white-slight"></div>
          </a>
        </div>
        <div class="card-body card-body-cascade text-center">
          <a href="" class="grey-text">
            <h5>Bolo de iorgute com amora</h5>
          </a>
          <div class="card-footer px-1">
            <span class="float-left font-weight-bold">
          </a>
            <strong>Pequeno: 12,00$</strong>
              <br />
              <strong>Grande: 24,00$</strong>
            </span>
           
          </div>
        </div>
      </div>
      
      
    </div>
    </div>
</div>
<div class="col-lg-3 col-md-6 mb-md-0 mb-4">
  <div class="card mb-4">
    <div class="view overlay">
    <div class="card card-cascade narrower card-ecommerce">
        <div class="view view-cascade overlay">
        <img class="card-img-top" src="<?= base_url('assets/mdb/img/Imagens/bolo-maracuja.jpg') ?>" alt="Card image cap">
        <div class="mask rgba-white-slight"></div>
            <div class="mask rgba-white-slight"></div>
          </a>
        </div>
        <div class="card-body card-body-cascade text-center">
          <a href="" class="grey-text">
            <h5>Bolo de iorgute com maracujá</h5>
          </a>
          <div class="card-footer px-1">
            <span class="float-left font-weight-bold">
          </a>
            <strong>Pequeno: 12,00$</strong>
              <br />
              <strong>Grande: 24,00$</strong>
            </span>
           
          </div>
        </div>
      </div>
      
      
    </div>
    </div>
</div>
<div class="col-lg-3 col-md-6 mb-md-0 mb-4">
  <div class="card mb-4">
    <div class="view overlay">
    <div class="card card-cascade narrower card-ecommerce">
        <div class="view view-cascade overlay">
        <img class="card-img-top" src="<?= base_url('assets/mdb/img/Imagens/bolo-churros.jpg') ?>" alt="Card image cap">
        <div class="mask rgba-white-slight"></div>
            <div class="mask rgba-white-slight"></div>
          </a>
        </div>
        <div class="card-body card-body-cascade text-center">
          <a href="" class="grey-text">
            <h5>Bolo de Churros</h5>
          </a>
          <div class="card-footer px-1">
            <span class="float-left font-weight-bold">
          </a>
            <strong>Pequeno: 15,00$</strong>
              <br />
              <strong>Grande: 30,00$</strong>
            </span>
            
          </div>
        </div>
      </div>
      
      
    </div>
    </div>
</div>
<div class="col-lg-3 col-md-6 mb-md-0 mb-4">
  <div class="card mb-4">
    <div class="view overlay">
    <div class="card card-cascade narrower card-ecommerce">
        <div class="view view-cascade overlay">
        <img class="card-img-top" src="<?= base_url('assets/mdb/img/Imagens/bolo-laranja.jpg') ?>" alt="Card image cap">
        <div class="mask rgba-white-slight"></div>
            <div class="mask rgba-white-slight"></div>
          </a>
        </div>
        <div class="card-body card-body-cascade text-center">
          <a href="" class="grey-text">
            <h5>Bolo de laranja</h5>
          </a>
          <div class="card-footer px-1">
            <span class="float-left font-weight-bold">
            <strong>Pequeno: 10,00$</strong>
              <br />
              <strong>Grande: 16,00$</strong>
            </span>
            
          </div>
        </div>
      </div>
      
      
    </div>
    </div>
</div>
<div class="col-lg-3 col-md-6 mb-md-0 mb-4">
  <div class="card mb-4">
    <div class="view overlay">
    <div class="card card-cascade narrower card-ecommerce">
        <div class="view view-cascade overlay">
        <img class="card-img-top" src="<?= base_url('assets/mdb/img/Imagens/bolo-gotas.jpg') ?>" alt="Card image cap">
          
        <div class="mask rgba-white-slight"></div>
      
            <div class="mask rgba-white-slight"></div>
          </a>
        </div>
        <div class="card-body card-body-cascade text-center">
          <a href="" class="grey-text">
            <h5>Bolo de cenoura com gotas de chocolate</h5>
          </a>
          <div class="card-footer px-1">
            <span class="float-left font-weight-bold">
            <strong>Pequeno: 12,00$</strong>
              <br />
              <strong>Grande: 22,00$</strong>
            </span>
            
          </div>
        </div>
      </div>
      
      
    </div>
    </div>
</div>
</div>
</section>
</div>
<br />
<section class="text-center my-5">
<br>
<div id="portfolio-heading" class="portfolio-heading section-heading">Bolos Já Encomendados para Festas</div>
<br>
<div class="conatiner">
<div class="card-group">
<div class="col-lg-3 col-md-6 mb-md-0 mb-4">
  <div class="card mb-4">
    <div class="view overlay">
    <div class="card card-cascade narrower card-ecommerce">
        <div class="view view-cascade overlay">
        <img class="card-img-top" src="<?= base_url('assets/mdb/img/Imagens/bolo_floresta.jpg') ?>" alt="Card image cap">
        <div class="mask rgba-white-slight"></div>
            <div class="mask rgba-white-slight"></div>
          </a>
        </div>
        <div class="card-body card-body-cascade text-center">
          <a  class="grey-text">
            <h5>Bolo personalizado</h5>
          </a>
          <div class="card-footer px-1">
            <span class="float-left font-weight-bold">
            <strong><a href="https://api.whatsapp.com/send?phone=5511988588235&text=Olá Gostaria de saber o preço e o sabor desse bolo">Preços e sabores a consultar</a></strong>
            </span>
          </div>
        </div>
      </div>
</div>
    </div>
  </div>
  <br /><br />
  <div class="col-lg-3 col-md-6 mb-md-0 mb-4">
  <div class="card mb-4">
    <div class="view overlay">
    <div class="card card-cascade narrower card-ecommerce">
        <div class="view view-cascade overlay">
        <img class="card-img-top" src="<?= base_url('assets/mdb/img/Imagens/bolo-niver.jpg') ?>" alt="Card image cap">
        <div class="mask rgba-white-slight"></div>
        </div>
        <div class="card-body card-body-cascade text-center">
          <a  class="grey-text">
          <h5>Bolo personalizado</h5>
          </a>
          <div class="card-footer px-1">
            <span class="float-left font-weight-bold">
            <strong><a href="https://api.whatsapp.com/send?phone=5511988588235&text=Olá Gostaria de saber o preço e o sabor desse bolo">Preços e sabores a consultar</a></strong>
            </span>
          </div>
        </div>
      </div>
      
    </div>
</div>
  </div>
<div class="col-lg-3 col-md-6 mb-md-0 mb-4">
  <div class="card mb-4">
    <div class="view overlay">
    <div class="card card-cascade narrower card-ecommerce">
        <div class="view view-cascade overlay">
        <img class="card-img-top" src="<?= base_url('assets/mdb/img/Imagens/bolo-lol.jpg') ?>" alt="Card image cap">
        <div class="mask rgba-white-slight"></div>
            <div class="mask rgba-white-slight"></div>
          </a>
        </div>
        <div class="card-body card-body-cascade text-center">
          <a href="" class="grey-text">
            <h5>Bolo personalizado</h5>
          </a>
          <div class="card-footer px-1">
            <span class="float-left font-weight-bold">
            <strong><a href="https://api.whatsapp.com/send?phone=5511988588235&text=Olá Gostaria de saber o preço e o sabor desse bolo">Preços e sabores a consultar</a></strong>
            </span>
          </div>
        </div>
      </div>
      
      
    </div>
    </div>
</div>
<div class="col-lg-3 col-md-6 mb-md-0 mb-4">
  <div class="card mb-4">
    <div class="view overlay">
    <div class="card card-cascade narrower card-ecommerce">
        <div class="view view-cascade overlay">
        <img class="card-img-top" src="<?= base_url('assets/mdb/img/Imagens/bolo-pessego.jpg') ?>" alt="Card image cap">
        <div class="mask rgba-white-slight"></div>
            <div class="mask rgba-white-slight"></div>
          </a>
        </div>
        <div class="card-body card-body-cascade text-center">
          <a href="" class="grey-text">
            <h5>Bolo personalizado</h5>
          </a>
          <div class="card-footer px-1">
            <span class="float-left font-weight-bold">
            <strong><a href="https://api.whatsapp.com/send?phone=5511988588235&text=Olá Gostaria de saber o preço e o sabor desse bolo">Preços e sabores a consultar</a></strong>
            </span>
          </div>
        </div>
      </div>
      
      
    </div>
    </div>
</div>
<div class="col-lg-3 col-md-6 mb-md-0 mb-4">
  <div class="card mb-4">
    <div class="view overlay">
    <div class="card card-cascade narrower card-ecommerce">
        <div class="view view-cascade overlay">
        <img class="card-img-top" src="<?= base_url('assets/mdb/img/Imagens/bolo-branco3.jpg') ?>" alt="Card image cap">
        <div class="mask rgba-white-slight"></div>
            <div class="mask rgba-white-slight"></div>
          </a>
        </div>
        <div class="card-body card-body-cascade text-center">
          <a href="" class="grey-text">
            <h5>Bolo personalizado</h5>
          </a>
          <div class="card-footer px-1">
            <span class="float-left font-weight-bold">
            <strong><a href="https://api.whatsapp.com/send?phone=5511988588235&text=Olá Gostaria de saber o preço e o sabor desse bolo">Preços e sabores a consultar</a></strong>
            </span>
          </div>
        </div>
      </div>
</div>
    </div>
  </div>
  <br /><br />
  <div class="col-lg-3 col-md-6 mb-md-0 mb-4">
  <div class="card mb-4">
    <div class="view overlay">
    <div class="card card-cascade narrower card-ecommerce">
        <div class="view view-cascade overlay">
        <img class="card-img-top" src="<?= base_url('assets/mdb/img/Imagens/bolo-branco2.jpg') ?>" alt="Card image cap">
        <div class="mask rgba-white-slight"></div>
        </div>
        <div class="card-body card-body-cascade text-center">
          <a href="" class="grey-text">
            <h5>Bolo personalizado</h5>
          </a>
          <div class="card-footer px-1">
            <span class="float-left font-weight-bold">
            <strong><a href="https://api.whatsapp.com/send?phone=5511988588235&text=Olá Gostaria de saber o preço e o sabor desse bolo">Preços e sabores a consultar</a></strong>
            </span>
          </div>
        </div>
      </div>
      
    </div>
</div>
  </div>
<div class="col-lg-3 col-md-6 mb-md-0 mb-4">
  <div class="card mb-4">
    <div class="view overlay">
    <div class="card card-cascade narrower card-ecommerce">
        <div class="view view-cascade overlay">
        <img class="card-img-top" src="<?= base_url('assets/mdb/img/Imagens/bolo-branco.jpg') ?>" alt="Card image cap">
        <div class="mask rgba-white-slight"></div>
            <div class="mask rgba-white-slight"></div>
          </a>
        </div>
        <div class="card-body card-body-cascade text-center">
          <a href="" class="grey-text">
            <h5>Bolo personalizado</h5>
          </a>
          <div class="card-footer px-1">
            <span class="float-left font-weight-bold">
            <strong><a href="https://api.whatsapp.com/send?phone=5511988588235&text=Olá Gostaria de saber o preço e o sabor desse bolo">Preços e sabores a consultar</a></strong>
            </span>
          </div>
        </div>
      </div>
      
      
    </div>
    </div>
</div>
<div class="col-lg-3 col-md-6 mb-md-0 mb-4">
  <div class="card mb-4">
    <div class="view overlay">
    <div class="card card-cascade narrower card-ecommerce">
        <div class="view view-cascade overlay">
        <img class="card-img-top" src="<?= base_url('assets/mdb/img/Imagens/bolo_morango.jpg') ?>" alt="Card image cap">
        <div class="mask rgba-white-slight"></div>
            <div class="mask rgba-white-slight"></div>
          </a>
        </div>
        <div class="card-body card-body-cascade text-center">
          <a href="" class="grey-text">
            <h5>Bolo personalizado</h5>
          </a>
          <div class="card-footer px-1">
            <span class="float-left font-weight-bold">
            <strong><a href="https://api.whatsapp.com/send?phone=5511988588235&text=Olá Gostaria de saber o preço e o sabor desse bolo">Preços e sabores a consultar</a></strong>
            </span>
          </div>
        </div>
      </div>
      
      
    </div>
    </div>
</div>
<div class="col-lg-3 col-md-6 mb-md-0 mb-4">
  <div class="card mb-4">
    <div class="view overlay">
    <div class="card card-cascade narrower card-ecommerce">
        <div class="view view-cascade overlay">
        <img class="card-img-top" src="<?= base_url('assets/mdb/img/Imagens/bolo_maravilha.jpg') ?>" alt="Card image cap">
          
        <div class="mask rgba-white-slight"></div>
      
            <div class="mask rgba-white-slight"></div>
          </a>
        </div>
        <div class="card-body card-body-cascade text-center">
          <a href="" class="grey-text">
            <h5>Bolo personalizado mulher maravilha</h5>
          </a>
          <div class="card-footer px-1">
            <span class="float-left font-weight-bold">
            <strong><a href="https://api.whatsapp.com/send?phone=5511988588235&text=Olá Gostaria de saber o preço e o sabor desse bolo">Preços e sabores a consultar</a></strong>
            </span>
          </div>
        </div>
      </div>
      
      
    </div>
    </div>
</div>
<div class="col-lg-3 col-md-6 mb-md-0 mb-4">
  <div class="card mb-4">
    <div class="view overlay">
    <div class="card card-cascade narrower card-ecommerce">
        <div class="view view-cascade overlay">
        <img class="card-img-top" src="<?= base_url('assets/mdb/img/Imagens/bolo-sp.jpg') ?>" alt="Card image cap">
        <div class="mask rgba-white-slight"></div>
        </div>
        <div class="card-body card-body-cascade text-center">
          <a  class="grey-text">
          <h5>Bolo personalizado</h5>
          </a>
          <div class="card-footer px-1">
            <span class="float-left font-weight-bold">
            <strong><a href="https://api.whatsapp.com/send?phone=5511988588235&text=Olá Gostaria de saber o preço e o sabor desse bolo">Preços e sabores a consultar</a></strong>
            </span>
          </div>
        </div>
      </div>
      
    </div>
</div>
  </div>
<div class="col-lg-3 col-md-6 mb-md-0 mb-4">
  <div class="card mb-4">
    <div class="view overlay">
    <div class="card card-cascade narrower card-ecommerce">
        <div class="view view-cascade overlay">
        <img class="card-img-top" src="<?= base_url('assets/mdb/img/Imagens/bolo-sccp.jpg') ?>" alt="Card image cap">
        <div class="mask rgba-white-slight"></div>
            <div class="mask rgba-white-slight"></div>
          </a>
        </div>
        <div class="card-body card-body-cascade text-center">
          <a href="" class="grey-text">
            <h5>Bolo personalizado</h5>
          </a>
          <div class="card-footer px-1">
            <span class="float-left font-weight-bold">
            <strong><a href="https://api.whatsapp.com/send?phone=5511988588235&text=Olá Gostaria de saber o preço e o sabor desse bolo">Preços e sabores a consultar</a></strong>
            </span>
          </div>
        </div>
      </div>
      
      
    </div>
    </div>
</div>
<div class="col-lg-3 col-md-6 mb-md-0 mb-4">
  <div class="card mb-4">
    <div class="view overlay">
    <div class="card card-cascade narrower card-ecommerce">
        <div class="view view-cascade overlay">
        <img class="card-img-top" src="<?= base_url('assets/mdb/img/Imagens/bolo-niver50.jpg') ?>" alt="Card image cap">
        <div class="mask rgba-white-slight"></div>
            <div class="mask rgba-white-slight"></div>
          </a>
        </div>
        <div class="card-body card-body-cascade text-center">
          <a href="" class="grey-text">
            <h5>Bolo personalizado</h5>
          </a>
          <div class="card-footer px-1">
            <span class="float-left font-weight-bold">
            <strong><a href="https://api.whatsapp.com/send?phone=5511988588235&text=Olá Gostaria de saber o preço e o sabor desse bolo">Preços e sabores a consultar</a></strong>
            </span>
          </div>
        </div>
      </div>
</div>
    </div>
  </div>
  <br /><br />
  <div class="col-lg-3 col-md-6 mb-md-0 mb-4">
  <div class="card mb-4">
    <div class="view overlay">
    <div class="card card-cascade narrower card-ecommerce">
        <div class="view view-cascade overlay">
        <img class="card-img-top" src="<?= base_url('assets/mdb/img/Imagens/bolo-niver4.jpg') ?>" alt="Card image cap">
        <div class="mask rgba-white-slight"></div>
        </div>
        <div class="card-body card-body-cascade text-center">
          <a href="" class="grey-text">
            <h5>Bolo personalizado</h5>
          </a>
          <div class="card-footer px-1">
            <span class="float-left font-weight-bold">
            <strong><a href="https://api.whatsapp.com/send?phone=5511988588235&text=Olá Gostaria de saber o preço e o sabor desse bolo">Preços e sabores a consultar</a></strong>
            </span>
          </div>
        </div>
      </div>
      
    </div>
</div>
  </div>
<div class="col-lg-3 col-md-6 mb-md-0 mb-4">
  <div class="card mb-4">
    <div class="view overlay">
    <div class="card card-cascade narrower card-ecommerce">
        <div class="view view-cascade overlay">
        <img class="card-img-top" src="<?= base_url('assets/mdb/img/Imagens/bolo-niver3.jpg') ?>" alt="Card image cap">
        <div class="mask rgba-white-slight"></div>
            <div class="mask rgba-white-slight"></div>
          </a>
        </div>
        <div class="card-body card-body-cascade text-center">
          <a href="" class="grey-text">
            <h5>Bolo personalizado</h5>
          </a>
          <div class="card-footer px-1">
            <span class="float-left font-weight-bold">
            <strong><a href="https://api.whatsapp.com/send?phone=5511988588235&text=Olá Gostaria de saber o preço e o sabor desse bolo">Preços e sabores a consultar</a></strong>
            </span>
          </div>
        </div>
      </div>
      
      
    </div>
    </div>
</div>
<div class="col-lg-3 col-md-6 mb-md-0 mb-4">
  <div class="card mb-4">
    <div class="view overlay">
    <div class="card card-cascade narrower card-ecommerce">
        <div class="view view-cascade overlay">
        <img class="card-img-top" src="<?= base_url('assets/mdb/img/Imagens/bolo-lindo2.jpg') ?>" alt="Card image cap">
        <div class="mask rgba-white-slight"></div>
            <div class="mask rgba-white-slight"></div>
          </a>
        </div>
        <div class="card-body card-body-cascade text-center">
          <a href="" class="grey-text">
            <h5>Bolo personalizado</h5>
          </a>
          <div class="card-footer px-1">
            <span class="float-left font-weight-bold">
            <strong><a href="https://api.whatsapp.com/send?phone=5511988588235&text=Olá Gostaria de saber o preço e o sabor desse bolo">Preços e sabores a consultar</a></strong>
            </span>
          </div>
        </div>
      </div>
      
      
    </div>
    </div>
</div>
<div class="col-lg-3 col-md-6 mb-md-0 mb-4">
  <div class="card mb-4">
    <div class="view overlay">
    <div class="card card-cascade narrower card-ecommerce">
        <div class="view view-cascade overlay">
        <img class="card-img-top" src="<?= base_url('assets/mdb/img/Imagens/bolo-floresta.jpg') ?>" alt="Card image cap">
        <div class="mask rgba-white-slight"></div>
            <div class="mask rgba-white-slight"></div>
          </a>
        </div>
        <div class="card-body card-body-cascade text-center">
          <a href="" class="grey-text">
            <h5>Bolo personalizado</h5>
          </a>
          <div class="card-footer px-1">
            <span class="float-left font-weight-bold">
            <strong><a href="https://api.whatsapp.com/send?phone=5511988588235&text=Olá Gostaria de saber o preço e o sabor desse bolo">Preços e sabores a consultar</a></strong>
            </span>
          </div>
        </div>
      </div>
      
      
    </div>
    </div>
</div>
<div class="col-lg-3 col-md-6 mb-md-0 mb-4">
  <div class="card mb-4">
    <div class="view overlay">
    <div class="card card-cascade narrower card-ecommerce">
        <div class="view view-cascade overlay">
        <img class="card-img-top" src="<?= base_url('assets/mdb/img/Imagens/bolo-noz.jpg') ?>" alt="Card image cap">
        <div class="mask rgba-white-slight"></div>
            <div class="mask rgba-white-slight"></div>
          </a>
        </div>
        <div class="card-body card-body-cascade text-center">
          <a href="" class="grey-text">
            <h5>Bolo personalizado</h5>
          </a>
          <div class="card-footer px-1">
            <span class="float-left font-weight-bold">
            <strong><a href="https://api.whatsapp.com/send?phone=5511988588235&text=Olá Gostaria de saber o preço e o sabor desse bolo">Preços e sabores a consultar</a></strong>
            </span>
          </div>
        </div>
      </div>
      
      
    </div>
    </div>
</div>
<div class="col-lg-3 col-md-6 mb-md-0 mb-4">
  <div class="card mb-4">
    <div class="view overlay">
    <div class="card card-cascade narrower card-ecommerce">
        <div class="view view-cascade overlay">
        <img class="card-img-top" src="<?= base_url('assets/mdb/img/Imagens/bolo-niver87.jpg') ?>" alt="Card image cap">
        <div class="mask rgba-white-slight"></div>
            <div class="mask rgba-white-slight"></div>
          </a>
        </div>
        <div class="card-body card-body-cascade text-center">
          <a href="" class="grey-text">
            <h5>Bolo personalizado</h5>
          </a>
          <div class="card-footer px-1">
            <span class="float-left font-weight-bold">
            <strong><a href="https://api.whatsapp.com/send?phone=5511988588235&text=Olá Gostaria de saber o preço e o sabor desse bolo">Preços e sabores a consultar</a></strong>
            </span>
          </div>
        </div>
      </div>
      
      
    </div>
    </div>
</div>
<div class="col-lg-3 col-md-6 mb-md-0 mb-4">
  <div class="card mb-4">
    <div class="view overlay">
    <div class="card card-cascade narrower card-ecommerce">
        <div class="view view-cascade overlay">
        <img class="card-img-top" src="<?= base_url('assets/mdb/img/Imagens/bolo-lindocont.jpg') ?>" alt="Card image cap">
        <div class="mask rgba-white-slight"></div>
            <div class="mask rgba-white-slight"></div>
          </a>
        </div>
        <div class="card-body card-body-cascade text-center">
          <a href="" class="grey-text">
            <h5>Bolo personalizado</h5>
          </a>
          <div class="card-footer px-1">
            <span class="float-left font-weight-bold">
            <strong><a href="https://api.whatsapp.com/send?phone=5511988588235&text=Olá Gostaria de saber o preço e o sabor desse bolo">Preços e sabores a consultar</a></strong>
            </span>
          </div>
        </div>
      </div>
      
      
    </div>
    </div>
</div>
<div class="col-lg-3 col-md-6 mb-md-0 mb-4">
  <div class="card mb-4">
    <div class="view overlay">
    <div class="card card-cascade narrower card-ecommerce">
        <div class="view view-cascade overlay">
        <img class="card-img-top" src="<?= base_url('assets/mdb/img/Imagens/bolo-red.jpg') ?>" alt="Card image cap">
        <div class="mask rgba-white-slight"></div>
            <div class="mask rgba-white-slight"></div>
          </a>
        </div>
        <div class="card-body card-body-cascade text-center">
          <a href="" class="grey-text">
            <h5>Bolo personalizado</h5>
          </a>
          <div class="card-footer px-1">
            <span class="float-left font-weight-bold">
            <strong><a href="https://api.whatsapp.com/send?phone=5511988588235&text=Olá Gostaria de saber o preço e o sabor desse bolo">Preços e sabores a consultar</a></strong>
            </span>
          </div>
        </div>
      </div>
      
      
    </div>
    </div>
</div>
</div>
</section>
</div>
<section class="text-center my-5">
<br>
<div id="portfolio-heading" class="portfolio-heading section-heading">Bolos no Pote</div>
<br>
<div class="conatiner">
<div class="card-group">
<div class="col-lg-3 col-md-6 mb-md-0 mb-4">
  <div class="card mb-4">
    <div class="view overlay">
    <div class="card card-cascade narrower card-ecommerce">
        <div class="view view-cascade overlay">
        <img class="card-img-top" src="<?= base_url('assets/mdb/img/Imagens/pote1.jpg') ?>" alt="Card image cap">
        <div class="mask rgba-white-slight"></div>
            <div class="mask rgba-white-slight"></div>
          </a>
        </div>
        <div class="card-body card-body-cascade text-center">
          <div class="grey-text">
            <h5>Sabor limão</h5>
</div>
<div class="card-footer px-1">
<span class="float-left font-weight-bold">
          
            <strong>Valor: 7,00$</strong>
            </span>
            
          </div>
        </div>
      </div>
</div>
    </div>
  </div>
  <br /><br />
  
<div class="col-lg-3 col-md-6 mb-md-0 mb-4">
  <div class="card mb-4">
    <div class="view overlay">
    <div class="card card-cascade narrower card-ecommerce">
        <div class="view view-cascade overlay">
        <img class="card-img-top" src="<?= base_url('assets/mdb/img/Imagens/pote3.jpg') ?>" alt="Card image cap">
        <div class="mask rgba-white-slight"></div>
        </div>
        <div class="card-body card-body-cascade text-center">
          <div class="grey-text">
          <h5>Pote Sensação</h5>
</div>
          <div class="card-footer px-1">
          <span class="float-left font-weight-bold">
            <strong>Valor: 7,00$</strong>
            </span>
           
          </div>
        </div>
      </div>
      
      
    </div>
    </div>
</div>
<div class="col-lg-3 col-md-6 mb-md-0 mb-4">
  <div class="card mb-4">
    <div class="view overlay">
    <div class="card card-cascade narrower card-ecommerce">
        <div class="view view-cascade overlay">
        <img class="card-img-top" src="<?= base_url('assets/mdb/img/Imagens/pote5.jpg') ?>" alt="Card image cap">
        <div class="mask rgba-white-slight"></div>
            <div class="mask rgba-white-slight"></div>
          </a>
        </div>
        <div class="card-body card-body-cascade text-center">
          <div class="grey-text">
          <h5>Pote Brigadeiro</h5>
</div>
          <div class="card-footer px-1">
            <span class="float-left font-weight-bold">
            <strong>Valor: 7,00$</strong>
            </span>
           
          </div>
        </div>
      </div>
</div>
    </div>
  </div>
  <br /><br />
  <div class="col-lg-3 col-md-6 mb-md-0 mb-4">
  <div class="card mb-4">
    <div class="view overlay">
    <div class="card card-cascade narrower card-ecommerce">
        <div class="view view-cascade overlay">
        <img class="card-img-top" src="<?= base_url('assets/mdb/img/Imagens/pote6.jpg') ?>" alt="Card image cap">
        <div class="mask rgba-white-slight"></div>
        </div>
        <div class="card-body card-body-cascade text-center">
          <div class="grey-text">
          <h5>Pote Red Ninho</h5>
</div>
          <div class="card-footer px-1">
            <span class="float-left font-weight-bold">
            <strong>Valor: 7,00$</strong>
            </span>
            
          </div>
        </div>
      </div>
      
    </div>
</div>
  </div>
<div class="col-lg-3 col-md-6 mb-md-0 mb-4">
  <div class="card mb-4">
    <div class="view overlay">
    <div class="card card-cascade narrower card-ecommerce">
        <div class="view view-cascade overlay">
        <img class="card-img-top" src="<?= base_url('assets/mdb/img/Imagens/pote7.jpg') ?>" alt="Card image cap">
        <div class="mask rgba-white-slight"></div>
          
        </div>
        <div class="card-body card-body-cascade text-center">
          <div class="grey-text">
          <h5>Pote Cenoura </h5>
          </a>
          <div class="card-footer px-1">
            <span class="float-left font-weight-bold">
            <strong>Valor: 7,00$</strong>
            </span>
            
          </div>
        </div>
      </div>
      
      
    </div>
    </div>
</div>

</div>
<div class="col-lg-3 col-md-6 mb-md-0 mb-4">
  <div class="card mb-4">
    <div class="view overlay">
    <div class="card card-cascade narrower card-ecommerce">
        <div class="view view-cascade overlay">
        <img class="card-img-top" src="<?= base_url('assets/mdb/img/Imagens/pote8.jpg') ?>" alt="Card image cap">
        <div class="mask rgba-white-slight"></div>
          
        </div>
        <div class="card-body card-body-cascade text-center">
          <div class="grey-text">
          <h5>Pote Churros </h5>
          </a>
          <div class="card-footer px-1">
            <span class="float-left font-weight-bold">
            <strong>Valor: 7,00$</strong>
            </span>
            
          </div>
        </div>
      </div>
      
      
    </div>
    </div>
</div>

</div>
</section>
</div>
<br />
<section class="text-center my-5">
<br>
<div id="portfolio-heading" class="portfolio-heading section-heading">Bolos Especiais</div>
<br>
<div class="conatiner">
<div class="card-group">
<div class="col-lg-3 col-md-6 mb-md-0 mb-4">
  <div class="card mb-4">
    <div class="view overlay">
    <div class="card card-cascade narrower card-ecommerce">
        <div class="view view-cascade overlay">
        <img class="card-img-top" src="<?= base_url('assets/mdb/img/Imagens/bolo_brigadeiro.jpg') ?>" alt="Card image cap">
        <div class="mask rgba-white-slight"></div>
            <div class="mask rgba-white-slight"></div>
          </a>
        </div>
        <div class="card-body card-body-cascade text-center">
          <div class="grey-text">
          <h5>Bolo de Brigadeiro </h5>
</div>
<div class="card-footer px-1">
<span class="float-left font-weight-bold">
          
<strong><a href="https://api.whatsapp.com/send?phone=5511988588235&text=Olá Gostaria de saber o preço e o sabor desse bolo">Preços e sabores a consultar</a></strong>
            </span>
            
          </div>
        </div>
      </div>
</div>
    </div>
  </div>
  <br /><br />
  
<div class="col-lg-3 col-md-6 mb-md-0 mb-4">
  <div class="card mb-4">
    <div class="view overlay">
    <div class="card card-cascade narrower card-ecommerce">
        <div class="view view-cascade overlay">
        <img class="card-img-top" src="<?= base_url('assets/mdb/img/Imagens/bolo-brigadeiro1.jpg') ?>" alt="Card image cap">
        <div class="mask rgba-white-slight"></div>
        </div>
        <div class="card-body card-body-cascade text-center">
          <div class="grey-text">
          <h5>Bolo de Brigadeiro </h5>
</div>
          <div class="card-footer px-1">
          <span class="float-left font-weight-bold">
          <strong><a href="https://api.whatsapp.com/send?phone=5511988588235&text=Olá Gostaria de saber o preço e o sabor desse bolo">Preços e sabores a consultar</a></strong>
            </span>
           
          </div>
        </div>
      </div>
      
      
    </div>
    </div>
</div>
<div class="col-lg-3 col-md-6 mb-md-0 mb-4">
  <div class="card mb-4">
    <div class="view overlay">
    <div class="card card-cascade narrower card-ecommerce">
        <div class="view view-cascade overlay">
        <img class="card-img-top" src="<?= base_url('assets/mdb/img/Imagens/bolo-brigadeiro2.jpg') ?>" alt="Card image cap">
        <div class="mask rgba-white-slight"></div>
            <div class="mask rgba-white-slight"></div>
          </a>
        </div>
        <div class="card-body card-body-cascade text-center">
          <div class="grey-text">
          
          <h5>Bolo de Brigadeiro </h5>
</div>
          <div class="card-footer px-1">
            <span class="float-left font-weight-bold">
            <strong><a href="https://api.whatsapp.com/send?phone=5511988588235&text=Olá Gostaria de saber o preço e o sabor desse bolo">Preços e sabores a consultar</a></strong>
            </span>
           
          </div>
        </div>
      </div>
</div>
    </div>
  </div>
  <br /><br />
  <div class="col-lg-3 col-md-6 mb-md-0 mb-4">
  <div class="card mb-4">
    <div class="view overlay">
    <div class="card card-cascade narrower card-ecommerce">
        <div class="view view-cascade overlay">
        <img class="card-img-top" src="<?= base_url('assets/mdb/img/Imagens/bolo-brigadeiro3.jpg') ?>" alt="Card image cap">
        <div class="mask rgba-white-slight"></div>
        </div>
        <div class="card-body card-body-cascade text-center">
          <div class="grey-text">
          <h5>Bolo de Brigadeiro </h5>
          
</div>
          <div class="card-footer px-1">
            <span class="float-left font-weight-bold">
            <strong><a href="https://api.whatsapp.com/send?phone=5511988588235&text=Olá Gostaria de saber o preço e o sabor desse bolo">Preços e sabores a consultar</a></strong>
            </span>
            
          </div>
        </div>
      </div>
      
    </div>
</div>
  </div>
<div class="col-lg-3 col-md-6 mb-md-0 mb-4">
  <div class="card mb-4">
    <div class="view overlay">
    <div class="card card-cascade narrower card-ecommerce">
        <div class="view view-cascade overlay">
        <img class="card-img-top" src="<?= base_url('assets/mdb/img/Imagens/bolo-castanha.jpg') ?>" alt="Card image cap">
        <div class="mask rgba-white-slight"></div>
          
        </div>
        <div class="card-body card-body-cascade text-center">
          <div class="grey-text">
          <h5>Bolo de Chocolate com Nozes </h5>
          </a>
          <div class="card-footer px-1">
            <span class="float-left font-weight-bold">
          </a>
            <strong>Pequeno: 12,00$</strong>
              <br />
              <strong>Grande: 22,00$</strong>
            </span>
           
          </div>
        </div>
      </div>
      
      
    </div>
    </div>
</div>

</div>
<div class="col-lg-3 col-md-6 mb-md-0 mb-4">
  <div class="card mb-4">
    <div class="view overlay">
    <div class="card card-cascade narrower card-ecommerce">
        <div class="view view-cascade overlay">
        <img class="card-img-top" src="<?= base_url('assets/mdb/img/Imagens/bolo-mel.jpg') ?>" alt="Card image cap">
        <div class="mask rgba-white-slight"></div>
          
        </div>
        <div class="card-body card-body-cascade text-center">
          <div class="grey-text">
          <h5>Bolo de Pão dd Mel </h5>
          </a>
          <div class="card-footer px-1">
            <span class="float-left font-weight-bold">
          </a>
            <strong>Pequeno: 15,00$</strong>
              <br />
              <strong>Grande: 30,00$</strong>
            </span>
           
          </div>
        </div>
      </div>
      
      
    </div>
    </div>
</div>

</div>
<div class="col-lg-3 col-md-6 mb-md-0 mb-4">
  <div class="card mb-4">
    <div class="view overlay">
    <div class="card card-cascade narrower card-ecommerce">
        <div class="view view-cascade overlay">
        <img class="card-img-top" src="<?= base_url('assets/mdb/img/Imagens/bolo-ninho.jpg') ?>" alt="Card image cap">
        <div class="mask rgba-white-slight"></div>
          
        </div>
        <div class="card-body card-body-cascade text-center">
          <div class="grey-text">
          <h5>Bolo de Leite Ninho </h5>
          </a>
          <div class="card-footer px-1">
            <span class="float-left font-weight-bold">
          </a>
            <strong>Pequeno: 15,00$</strong>
              <br />
              <strong>Grande: 30,00$</strong>
            </span>
           
          </div>
        </div>
      </div>
      
      
    </div>
    </div>
</div>

</div>
<div class="col-lg-3 col-md-6 mb-md-0 mb-4">
  <div class="card mb-4">
    <div class="view overlay">
    <div class="card card-cascade narrower card-ecommerce">
        <div class="view view-cascade overlay">
        <img class="card-img-top" src="<?= base_url('assets/mdb/img/Imagens/bolo-pacoca.jpg') ?>" alt="Card image cap">
        <div class="mask rgba-white-slight"></div>
          
        </div>
        <div class="card-body card-body-cascade text-center">
          <div class="grey-text">
          <h5>Bolo de Paçoca </h5>
          </a>
          <div class="card-footer px-1">
            <span class="float-left font-weight-bold">
          </a>
            <strong>Pequeno: 15,00$</strong>
              <br />
              <strong>Grande: 30,00$</strong>
            </span>
           
          </div>
        </div>
      </div>
      
      
      
    </div>
    </div>
</div>

</div>
</section>
</div>
<br />