		<br />
<section class="text-center my-5">
<br>
<div id="portfolio-heading" class="portfolio-heading section-heading">Doces</div>
<section class="text-center my-5">
<h3 class="h2-responsive  text-center my-5 animated swing">Aproveite nossa promoção, na compra de 1 pote a unidade sai por: R$ 15,00 e na compra de 2 potes sai por: R$ 25,00 as duas </h3>

</section>
<div class="conatiner">
<div class="card-group">
<div class="col-lg-3 col-md-6 mb-md-0 mb-4">
  <div class="card mb-4">
    <div class="view overlay">
    <div class="card card-cascade narrower card-ecommerce">
        <div class="view view-cascade overlay">
        <img class="card-img-top" src="<?= base_url('assets/mdb/img/Imagens/doce1.jpg') ?>" alt="Card image cap">
      <a href="#!">
        <div class="mask rgba-white-slight"></div>
      </a>
            <div class="mask rgba-white-slight"></div>
          </a>
        </div>
        <div class="card-body card-body-cascade text-center">
          <div class="grey-text">
            <h5>Abóbora com Coco</h5>
  </div>
          <div class="card-footer px-1">
            <span class="float-left font-weight-bold">
              <strong>Valor: 15,00$</strong>
            </span>
          </div>
        </div>
      </div>
</div>
    </div>
  </div>
  <br /><br />
  <div class="col-lg-3 col-md-6 mb-md-0 mb-4">
  <div class="card mb-4">
    <div class="view overlay">
    <div class="card card-cascade narrower card-ecommerce">
        <div class="view view-cascade overlay">
        <img class="card-img-top" src="<?= base_url('assets/mdb/img/Imagens/doce2.jpg') ?>" alt="Card image cap">
        
        <div class="mask rgba-white-slight"></div>
      </a>
        </div>
        <div class="card-body card-body-cascade text-center">
          <div class="grey-text">
            <h5>Batata Doce</h5>
  </div>
          <div class="card-footer px-1">
            <span class="float-left font-weight-bold">
            <strong>Valor: 15,00$</strong>
            </span>
          </div>
        </div>
      </div>
      
    </div>
</div>
  </div>
<div class="col-lg-3 col-md-6 mb-md-0 mb-4">
  <div class="card mb-4">
    <div class="view overlay">
    <div class="card card-cascade narrower card-ecommerce">
        <div class="view view-cascade overlay">
        <img class="card-img-top" src="<?= base_url('assets/mdb/img/Imagens/doce3.jpg') ?>" alt="Card image cap">
         
        <div class="mask rgba-white-slight"></div>
      </a>
            <div class="mask rgba-white-slight"></div>
          </a>
        </div>
        <div class="card-body card-body-cascade text-center">
          <div class="grey-text">
            <h5>Tipo Ambrosia</h5>
  </div>

          <div class="card-footer px-1">
            <span class="float-left font-weight-bold">
            <strong>Valor: 15,00$</strong>
            </span>
          </div>
        </div>
      </div>
      
      
    </div>
    </div>
</div>
<div class="col-lg-3 col-md-6 mb-md-0 mb-4">
  <div class="card mb-4">
    <div class="view overlay">
    <div class="card card-cascade narrower card-ecommerce">
        <div class="view view-cascade overlay">
        <img class="card-img-top" src="<?= base_url('assets/mdb/img/Imagens/doceabobora.jpg') ?>" alt="Card image cap">
            <div class="mask rgba-white-slight"></div>
          </a>
        </div>
        <div class="card-body card-body-cascade text-center">
          <div class="grey-text">
            <h5>Abóbora Cremosa</h5>
  </div>
         

          <div class="card-footer px-1">
            <span class="float-left font-weight-bold">
            <strong>Valor: 15,00$</strong>
            </span>
          </div>
        </div>
      </div>
</div>
    </div>
  </div>
  <br /><br />
  <div class="col-lg-3 col-md-6 mb-md-0 mb-4">
  <div class="card mb-4">
    <div class="view overlay">
    <div class="card card-cascade narrower card-ecommerce">
        <div class="view view-cascade overlay">
		<img class="card-img-top" src="<?= base_url('assets/mdb/img/Imagens/docedeleite.jpg') ?>" alt="Card image cap">
    <div class="mask rgba-white-slight"></div>
          </a>
        </div>
        <div class="card-body card-body-cascade text-center">
          <div class="grey-text">
            <h5>Doce de leite com coco</h5>
  </div>
          <div class="card-footer px-1">
            <span class="float-left font-weight-bold">
            <strong>Valor: 15,00$</strong>
            </span>
          </div>
        </div>
      </div>
      
    </div>
</div>
  </div>
<div class="col-lg-3 col-md-6 mb-md-0 mb-4">
  <div class="card mb-4">
    <div class="view overlay">
    <div class="card card-cascade narrower card-ecommerce">
        <div class="view view-cascade overlay">
		<img class="card-img-top" src="<?= base_url('assets/mdb/img/Imagens/cidracomcoco.jpg') ?>" alt="Card image cap">
            <div class="mask rgba-white-slight"></div>
          </a>
        </div>
        <div class="card-body card-body-cascade text-center">
          <div class="grey-text">
            <h5>Cidra com coco</h5>
  </div>

          <div class="card-footer px-1">
            <span class="float-left font-weight-bold">
            <strong>Valor: 15,00$</strong>
            </span>
          </div>
        </div>
      </div>
      
      
    </div>
    </div>
</div>
<div class="col-lg-3 col-md-6 mb-md-0 mb-4">
  <div class="card mb-4">
    <div class="view overlay">
    <div class="card card-cascade narrower card-ecommerce">
        <div class="view view-cascade overlay">
		<img class="card-img-top" src="<?= base_url('assets/mdb/img/Imagens/geleia.jpg') ?>" alt="Card image cap">
            <div class="mask rgba-white-slight"></div>
          </a>
        </div>
        <div class="card-body card-body-cascade text-center">
          <div class="grey-text">
            <h5>Geléia de frutas vermelhas</h5>
  </div>

          <div class="card-footer px-1">
            <span class="float-left font-weight-bold">
            <strong>Valor: 15,00$</strong>
            </span>
          </div>
        </div>
      </div>
      
      
    </div>
    </div>
</div>
<div class="col-lg-3 col-md-6 mb-md-0 mb-4">
  <div class="card mb-4">
    <div class="view overlay">
    <div class="card card-cascade narrower card-ecommerce">
        <div class="view view-cascade overlay">
		<img class="card-img-top" src="<?= base_url('assets/mdb/img/Imagens/goiabada.jpg') ?>" alt="Card image cap">
            <div class="mask rgba-white-slight"></div>
          </a>
        </div>
        <div class="card-body card-body-cascade text-center">
          <div class="grey-text">
            <h5>Goiaba em calda</h5>
  </div>

          <div class="card-footer px-1">
            <span class="float-left font-weight-bold">
            <strong>Valor: 15,00$</strong>
            </span>
          </div>
        </div>
      </div>
      
      
    </div>
    </div>
</div>
</div>
</section>
</div>
