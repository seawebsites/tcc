<div class="modal fade" id="modalContactForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header text-center">
                <h4 class="modal-title w-100 font-weight-bold">Cadastre-se</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body mx-3">
                <form method="POST" id="form_cadastrofidel">
                  <div class="form-group">
                    <input type="text" id="nome_fidel" name="nome_fidel" class="form-control form-control-sm">
                    <label data-error="wrong" data-success="right" for="form34">Seu Nome</label>
                  </div>
                  <div class="form-group">
                    <input type="email" id="email_fidel" name="email_fidel" class="form-control form-control-sm">
                    <label data-error="wrong" data-success="right" for="form29">Seu E-mail</label>
                  </div>
                  <div class="form-group">
                    <input type="cpf" id="cpf" name="cpf" class="form-control form-control-sm">
                    <label data-error="wrong" data-success="right" for="form32">Seu CPF</label>
                  </div>
                </form>
              </div>
              <div class="modal-footer d-flex justify-content-center">
                <button type="submit" id="enviar" class="btn btn purple-text" onclick="document.getElementById('form_cadastrofidel').submit();" style=" background-color: #20B2AA;">
                  Enviar
                </button>
              </div>
            </div>
          </div>
        </div>