<div class="container">
    <div class="row">
        <div class="col-md-10 mx-auto mt-4">
            <table class="table">
                <thead>
                    <tr>
                        <th scope="col">Nome</th>
                        <th scope="col">E-mail</th>
                        <th scope="col">CPF</th>
                    </tr>
                </thead>
                <tbody>
                    <?= $table ?>
                </tbody>
            </table>
        </div>
    </div>
</div>