<br />
<div id="fotos" class="container shadow-lg p-3 mb-5 bg-white rounded">
  <div class="row">

    <section class="my-5">
      <h3 class="h2-responsive font-weight-bold text-center my-5">Como Funciona Nosso Sistema de Fidelidade </h3>
      <p class="grey-text text-center w-responsive mx-auto mb-5">Nosso sistema consiste em que, na compra de um dos nossos bolos grandes, nossos clientes irão sendo pontuados até atingir uma máxima de dez pontos.
        Atingida essa máxima, trocará os pontos adquiridos por um bolo grande com o sabor sa sua escolha em nossa loja.
        <br />
        Para se tornar um participante basta se inscrever no formulário abaixo, preenchendo os campos e efetuando seu cadastro. Participe agora mesmo!
        <div class="modal fade" id="modalContactForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header text-center">
                <h4 class="modal-title w-100 font-weight-bold">Cadastre-se</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body mx-3">
                <form method="POST" id="form_cadastrofidel">
                  <div class="form-group">
                    <input type="text" id="nome" name="nome" class="form-control form-control-sm">
                    <label data-error="wrong" data-success="right" for="form34">Seu Nome</label>
                  </div>
                  <div class="form-group">
                    <input type="email" id="email" name="email" class="form-control form-control-sm">
                    <label data-error="wrong" data-success="right" for="form29">Seu E-mail</label>
                  </div>
                  <div class="form-group">
                    <input type="cpf" id="cpf" name="cpf" class="form-control form-control-sm">
                    <label data-error="wrong" data-success="right" for="form32">Seu CPF</label>
                  </div>
                </form>
              </div>
              <div class="modal-footer d-flex justify-content-center">
                <button type="submit" id="enviar" class="btn btn purple-text" onclick="document.getElementById('form_cadastrofidel').submit();" style=" background-color: #20B2AA;">
                  Enviar
                </button>
              </div>
            </div>
          </div>
        </div>
        <div class="text-center mt-4 mb-2">
          <div class="text-center">
            <a href="" class="btn btn-default btn-rounded mb-4" data-toggle="modal" data-target="#modalContactForm">Gostou? Cadastre-se e participe você também</a>
          </div>
        </div>
    </section>

  </div>
</div>
<br />