					
		<div id="container" style="width: 900px; margin: 0 auto;">
			
 
			
			<p style="text-align: center;"><a href="<?= base_url('index.php/Produtos')?>">Continuar Comprando</a></p>
			<?php echo form_open('carrinho/atualizar'); ?>
			<p><strong>Total de Itens no Carrinho:</strong> <?= $this->cart->total_items() ?></p>
			<p>Para excluir um produto, informe quantidade 0 e clique no botão Atualizar.</p>
			<table cellpadding="6" cellspacing="1" style="width:100%" border="1" align="center">
			
			<tr>
			  <th>Quantidade</th>
			  <th>Produto</th>
			  <th style="text-align:right">Preço</th>
			  <th style="text-align:right">Sub-Total</th>
			</tr>
			
			<?php $i = 1; ?>
			
			<?php foreach ($this->cart->contents() as $items): ?>
			
				<?php echo form_hidden($i.'[rowid]', $items['rowid']); ?>
			
				<tr>
				  <td><?php echo form_input(array('name' => $i.'[qty]', 'value' => $items['qty'], 'maxlength' => '3', 'size' => '5')); ?></td>
				  <td style="text-align: left;">
					<?php echo $items['name']; ?>
			
						<?php if ($this->cart->has_options($items['rowid']) == TRUE): ?>
			
							<p>
								<?php foreach ($this->cart->product_options($items['rowid']) as $option_name => $option_value): ?>
			
									<strong><?php echo $option_name; ?>:</strong> <?php echo $option_value; ?><br />
			
								<?php endforeach; ?>
							</p>
			
						<?php endif; ?>
			
				  </td>
				  <td style="text-align:right">R$ <?php echo moeda($items['price']); ?></td>
				  <td style="text-align:right">R$ <?php echo moeda($items['subtotal']); ?></td>
				</tr>
			
			<?php $i++; ?>
			
			<?php endforeach; ?>
			
			<tr>
			  <td colspan="2">&nbsp;</td>
			  <td class="left"><strong>Total</strong></td>
			  <td class="right">R$ <?php echo moeda($this->cart->total()); ?></td>
			</tr>
			<tr>
				<td colspan="4">
					<div style="float: left; padding-top: 6px;"><a href="<?= base_url('index.php/carrinho/limpar')?>">Limpar Carrinho</a></div>
					<div style="float: right;"><?php echo form_submit('', 'Atualizar'); ?></div>
				</td>
			</tr>
			</table>
		</div>