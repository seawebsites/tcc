<script type="text/javascript" src="<?= base_url('assets/mdb/js/jquery-3.3.1.min.js') ?>"></script>
<script type="text/javascript" src="<?= base_url('assets/mdb/js/popper.min.js') ?>"></script>
<script type="text/javascript" src="<?= base_url('assets/mdb/js/bootstrap.min.js') ?>"></script>
<script type="text/javascript" src="<?= base_url('assets/mdb/js/mdb.js') ?>"></script>
<script type="text/javascript" src="<?= base_url('assets/mdb/js/lightbox.js') ?>"></script>
<script type="text/javascript" src="<?= base_url('assets/mdb/js/cep.js') ?>"></script>
<div>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css"><a href="https://api.whatsapp.com/send?phone=5511988588235&text=Olá Gostaria de realizar uma encomenda" Style="	position:fixed;width:60px;height:60px;bottom:40px;right:40px;background-color:#25d366;color:#FFF;border-radius:50px;text-align:center;font-size:30px;box-shadow: 1px 1px 2px #888;
  z-index:1000;" target="_blank">
        <i style="margin-top:16px" class="fa fa-whatsapp"></i>
    </a>
    </body>

    <footer class="page-footer font-small " style="background-color: #20B2AA;">
        <div style="background-color: #BA55D3;">
            <div class="container">
                <div class="row py-4 d-flex align-items-center">
                    <div class="col-md-6 col-lg-5 text-center text-md-left mb-4 mb-md-0">
                        <h6 class="mb-0">Conecte-se conosco nas redes sociais!</h6>
                    </div>
                    <div class="col-md-6 col-lg-7 text-center text-md-right">
                        <a href="https://www.facebook.com/cake360casadebolo/" class="fb-ic">
                            <i class="fab fa-facebook-f white-text mr-4"> </i>
                        </a>
                        <a href="https://www.instagram.com/cake360casadebolo/?hl=pt-br" class="ins-ic">
                            <i class="fab fa-instagram white-text"> </i>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="container text-center text-md-left mt-5 d-none d-lg-block">
            <div class="row mt-3">
                <div class="d-flex justify-content-lg-center">
                    <div class="col-md-3 col-lg-4 col-xl-3 mx-auto mb-4">
                        <h6 class="text-uppercase font-weight-bold">Cake 360 - Casa de Bolo</h6>
                        <hr class="deep-purple accent-2 mb-4 mt-0 d-inline-block mx-auto" style="width: 100px;">
                        <p><a href="#modal" class="nav-link" data-toggle="modal" data-target="#modalRegisterForm">
                                <img src="<?= base_url('assets/mdb/img/Imagens/logo.png') ?>" height="75" alt="Responsive image"></a>
                            <p class="text-uppercase font-weight-bold">Horário de atendimento:</p>
                            <p>Terça à domingo das 9:00hrs às 19:00hrs</p>
                        </p>
                        </a>
                    </div>
                    <div class="col-md-2 col-lg-2 col-xl-2 mx-auto mb-4">
                        <h6 class="text-uppercase font-weight-bold">Produtos</h6>
                        <hr class="deep-purple accent-2 mb-4 mt-0 d-inline-block mx-auto" style="width: 60px;">
                        <p>
                            <a href="<?= base_url('index.php/produto/bolos') ?>">Bolos</a>
                        </p>
                        <p>
                            <a href="<?= base_url('index.php/produto/tortas') ?>">Salgados</a>
                        </p>
                        <p>
                            <a href="<?= base_url('index.php/produto/bebidas') ?>">Bebidas</a>
                        </p>
                    </div>
                    <div class="col-md-3 col-lg-2 col-xl-2 mx-auto mb-4">
                        <h6 class="text-uppercase font-weight-bold">Outros</h6>
                        <hr class="deep-purple accent-2 mb-4 mt-0 d-inline-block mx-auto" style="width: 60px;">
                        <p>
                            <a href="<?= base_url('index.php/inicio') ?>">Página Inicial</a>
                        </p>
                        <p>
                            <a href="<?= base_url('index.php/galeria') ?>">Galeria de Fotos</a>
                        </p>
                        <p>
                            <a href="<?= base_url('index.php/encomenda') ?>">Encomendas</a>
                        </p>
                        <p>
                            <a href="<?= base_url('index.php/sobre') ?>">Sobre</a>
                        </p>
                    </div>
                    <div class="col-md-4 col-lg-3 col-xl-3 mx-auto mb-md-0 mb-4">
                        <h6 class="text-uppercase font-weight-bold">Contato</h6>
                        <hr class="deep-purple accent-2 mb-4 mt-0 d-inline-block mx-auto" style="width: 60px;">
                        <p>
                            <i class="fas fa-home mr-3"></i> Av Salgado Filho, 3.242 - Centro - Guarulhos - SP</p>
                        <p>
                            <i class="fas fa-envelope mr-3"></i>cake360casadebolo@gmail.com</p>
                        <p>
                            <i class="fas fa-phone mr-3"></i> +55 11 4574-1905</p>
                        <p>
                            <i class="fab fa-whatsapp mr-3"></i> +55 11 98858-8235</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="footer-copyright text-center py-3">
            CNPJ 28.471.937/0001-64</div>

    </footer>

    </html>