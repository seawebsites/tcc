<nav class="navbar fixed-top navbar-expand-lg navbar-dark scrolling-navbar">
  <div class="container">
    <a class="navbar-brand">
      <img src="<?= base_url('assets/mdb/img/Imagens/logo.png') ?>" height="55" alt="Responsive image">
    </a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
      <ul class="navbar-nav ml-auto">
        <li class="nav-item">
          <a class="nav-link" href="<?= base_url('index.php/inicio') ?>">Página Inicial
            <span class="sr-only">(current)</span></a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="<?= base_url('index.php/galeria') ?>">Galeria de Fotos</a>
        </li>
        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Produtos</a>
          <div class="dropdown-menu dropdown-primary" aria-labelledby="navbarDropdownMenuLink">
            <a class="dropdown-item" href="<?= base_url('index.php/produto/bolos') ?>">Bolos</a>
            <a class="dropdown-item" href="<?= base_url('index.php/produto/tortas') ?>">Tortas</a>
            <a class="dropdown-item" href="<?= base_url('index.php/produto/bebidas') ?>">Bebidas</a>
            <a class="dropdown-item" href="<?= base_url('index.php/produto/doces') ?>">Doces</a>
          </div>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="<?= base_url('index.php/encomenda') ?>">Realize sua Encomenda</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="<?= base_url('index.php/sobre') ?>">Sobre</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="<?= base_url('index.php/contato') ?>">Contato</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="#modal" data-toggle="modal" data-target="#modalExemplo">Entrar</a>
        </li>
      </ul>
    </div>
</nav>