<nav id="cor" class="navbar navbar-expand-lg navbar-dark">
  <div class="container">
  <a class="navbar-brand" href="#">
      <img src="<?= base_url('assets/mdb/img/Imagens/logo.png') ?>" height="55" alt="Responsive image">
    </a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#basicExampleNav"
    aria-controls="basicExampleNav" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="basicExampleNav">
    <ul class="navbar-nav mr-auto">
      </li>
      <li class="nav-item">
      <a class="nav-link" href="<?= base_url('index.php/produto/lista') ?>">Produtos Cadastrados</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="<?= base_url('index.php/Encomenda/listar') ?>">Ver Pedidos</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="<?= base_url('index.php/Fidelizacao/listar') ?>">Lista de Clientes</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="<?= base_url('index.php/auth/logout') ?>">Sair</a>
      </li>
    </ul>
  </div>

  </div>
</nav>