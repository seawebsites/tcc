<div id="carousel" class="carousel slide carousel-fade" data-ride="carousel">
  <ol class="carousel-indicators">
    <li data-target="#carousel" data-slide-to="0" class="active"></li>
    <li data-target="#carousel" data-slide-to="1"></li>
    <li data-target="#carousel" data-slide-to="2"></li>
    <li data-target="#carousel" data-slide-to="3"></li>
    <li data-target="#carousel" data-slide-to="4"></li>
    <li data-target="#carousel" data-slide-to="5"></li>
  </ol>
  <div class="carousel-inner" role="listbox">
    <div class="carousel-item active">
      <div class="view">
        <div class="image">
          <img class="img-fluid d-block" src="<?= base_url('assets\mdb\img\Imagens\noname.png') ?>" alt="Primeiro slide">
          <a href="https://www.freepik.com/free-photos-vectors/background"></a>
          <div class="mask rgba-black-light"></div>
        </div>
        <div class="carousel-caption py-5 text-center">
          <h1 class="h1-responsive">Bem vindo a Cake 360!</h1>
          <h3>Casa de Bolo</h3>
        </div>
      </div>
    </div>
    <div class="carousel-item">     
      <div class="view">
        <img class="img-fluid d-block" src="<?= base_url('assets\mdb\img\Imagens\noname4.png') ?>" alt="Segundo slide">
        <a href="https://www.freepik.com/free-photos-vectors/light"></a>
        <div class="mask rgba-black-light"></div>
      </div>
      <div class="carousel-caption py-5 text-left d-none d-md-block">
        <h1 class="h1-responsive">Horário de Atendimento:</h1>
        <h3>Terça à domingo das 9:00hrs às 19:00hrs</h3>
      </div>
    </div>
    <div class="carousel-item">
      <div class="view">
        <img class="img-fluid d-block" src="<?= base_url('assets\mdb\img\Imagens\bolo_festa.png') ?>" alt="Terceiro slide">
        <a href="https://www.freepik.com/free-photos-vectors/light"></a>
        <div class="mask rgba-black-light"></div>
      </div>
      <div class="carousel-caption py-5 text-left d-none d-md-block">
        <h1 class="h1-responsive">Encomende o seu bolo com a gente!</h1>
        <h3>Personalizamos bolos para aniversários, batizado, chá de bebê e outros eventos.</h3>
      </div>
    </div>
    <div class="carousel-item">
      <div class="view">
        <img class="img-fluid d-block" src="<?= base_url('assets\mdb\img\Imagens\noname3.png') ?>" alt="Quarto slide">
        <div class="mask rgba-black-light"></div>
      </div>
      <div class="carousel-caption py-5 text-left">
        <h1 class="h1-responsive">Venha nos visitar!</h1>
        <h3>Venha conhecer nossos funcionários e estabelecimento pessoalmente.</h3>
      </div>
    </div>
    <div class="carousel-item">
      <div class="view">
        <img class="img-fluid d-block" src="<?= base_url('assets\mdb\img\Imagens\montagempascoa.jpg') ?>" alt="Quinto slide">
        <div class="mask rgba-black-light"></div>
      </div>
      <div class="carousel-caption py-5 text-left">
        <h1 class="h1-responsive">Nossa Promoção de Páscoa!</h1>
        <h3>Dona da loja junto com a ganhadora do sorteio de páscoa</h3>
      </div>
    </div>
    <div class="carousel-item">
      <div class="view">
      <a href="<?= base_url('index.php/Fidelizacao') ?>" target=”_blank” title="Temos Bolos Personalizados"><img class="img-fluid d-block" src="<?= base_url('assets\mdb\img\Imagens\fidelidade.jpg') ?>" alt="Sexto slide"></a>
      </div>
    </div>
  </div>
</div>
<a class="carousel-control-prev" href="#carousel" role="button" data-slide="prev">
  <span class="carousel-control-prev-icon" aria-hidden="true"></span>
  <span class="sr-only">Previous</span>
</a>
<a class="carousel-control-next" href="#carousel" role="button" data-slide="next">
  <span class="carousel-control-next-icon" aria-hidden="true"></span>
  <span class="sr-only">Next</span>
</a>


<div id="features" class="features">
  <div class="container">
    <div id="features-heading" class="features-heading section-heading animated swing">
      Aqui você encontra</div>
  </div>
</div>
<div class="container">
  <div class="row">
    <div class="d-flex justify-content-md-between">
      <div class="col-sm">
        <div class="text-center">
          <p><img src="https://img.icons8.com/dusk/80/000000/birthday-cake.png" class="animated jackInTheBox"></p>
          <p>
            <h5>Bolos, tortas, chás e muito mais.</h5>
          </p>
        </div>
      </div>
      <div class="col-sm">
        <div class="text-center">
        <a href="<?= base_url('index.php/encomenda') ?>"><p><img class="animated jackInTheBox" src="https://img.icons8.com/dusk/80/000000/phone.png"></p></a>
          <p>
            <h5>Aceitamos Encomendas</h5>
          </p>
        </div>
      </div>
      <div class="col-sm">
        <div class="text-center">
        <a href="<?= base_url('index.php/contato') ?>"><p><img src="https://img.icons8.com/dusk/80/000000/marker.png" class="animated jackInTheBox"></p></a>
          <p>
            <h5>Bolos para degustação em nossa loja</h5>
          </p>
        </div>
      </div>
      <div class="col-sm">
        <div class="text-center">
          <p><img src="https://img.icons8.com/dusk/80/000000/deliver-food.png" class="animated jackInTheBox"data-toggle="tooltip" title="Hooray!"></p>
          <p>
            <h5>Realizamos entregas</h5>
          </p>
        </div>
      </div>
    </div>
  </div>
</div>

<br> <br>

<div id="tagline" class="tagline dark">
  <div class="container animated lightSpeedIn">Os melhores Bolos e Doces estão aqui!</div>
</div>

<br> <br>

<div id="portfolio-heading" class="portfolio-heading section-heading">Algumas fotos...</div>

<br> <br>

<div class="container">
  <div class="row">
    <div class="d-flex justify-content-md-center">
      <div class="col-4 col-md-4">
        <div class="view overlay">
          <a href="<?= base_url('index.php/produto/bolos') ?>" target=”_blank” title="Temos Bolos Personalizados"><img class="img-fluid" alt="Responsive image" width="600" height="400" src="<?= base_url('assets/mdb/img/Imagens/bolo_maravilha.jpg') ?>" /></a>
        </div>
      </div>
      <div class="col-4 col-md-4">
        <div class="view overlay">
          <a href="<?= base_url('index.php/produto/bolos') ?>" target=”_blank” title="Temos Bolos Caseiros"><img class="img-fluid" alt="Responsive image" width="600" height="400" src="<?= base_url('assets/mdb/img/Imagens/bolo_goiabada.jpg') ?>" /></a>
        </div>
      </div>
      <div class="col-4 col-md-4">
        <div class="view overlay">
          <a href="<?= base_url('index.php/produto/bolos') ?>" target=”_blank” title="Encomende com a gente"><img class="img-fluid" alt="Responsive image" width="600" height="400" src="<?= base_url('assets/mdb/img/Imagens/bolo_floresta.jpg') ?>" /></a>
        </div>
      </div>
    </div>
    <br>
    <div class="d-flex justify-content-md-center">
      <div class="col-4 col-md-4">
        <div class="view overlay">
          <a href="<?= base_url('index.php/produto/bolos') ?>" target=”_blank” title="Deliciosos bolos no pote"><img class="img-fluid" alt="Responsive image" width="600" height="400" src="<?= base_url('assets/mdb/img/Imagens/bolo_pote.jpg') ?>" /></a>
        </div>
      </div>
      <div class="col-4 col-md-4">
        <div class="view overlay">
          <a href="<?= base_url('index.php/produto/bolos') ?>" target=”_blank” title="Deliciosas tortas doces"><img class="img-fluid" alt="Responsive image" width="600" height="400" src="<?= base_url('assets/mdb/img/Imagens/torta_doce.jpg') ?>" /></a>
        </div>
      </div>
      <div class="col-4 col-md-4">
        <div class="view overlay">
          <a href="<?= base_url('index.php/produto/salgados') ?>" target=”_blank” title="Deliciosas tortas salgadas"><img class="img-fluid" alt="Responsive image" width="600" height="400" src="<?= base_url('assets/mdb/img/Imagens/torta_salgada.jpg') ?>" /></a>
        </div>
      </div>
    </div>
  </div>
</div>
<br> <br>



<div id="portfolio-heading " class="portfolio-heading section-heading">Avaliações</div>

<div class="container">
  <div class="row">
    <ul class="list-unstyled">
      <li class="media">
        <img class="d-flex mr-3" src="<?= base_url('assets/mdb/img/Imagens/logo.png') ?>" style="width: 100px">
        <div class="media-body">
          <h5 class="mt-0 font-weight-bold">Pamela Souza</h5>
          <p class="dark-grey-text mt-4"><i class="fas fa-quote-left pr-2"></i>Eu não tive tempo de mandar msg antes, mas gostaria de elogiar seu bolo, um dos melhores que eu
            já comi na minha vida, sem falar que o atendimento foi maravilhoso. Vocês são demais.
          </p>
      </li>
      <li class="media my-4">
        <img class="d-flex mr-3" src="<?= base_url('assets/mdb/img/Imagens/logo.png') ?>" style="width: 100px">
        <div class="media-body">
          <h5 class="mt-0 font-weight-bold">Ewellin Akioka</h5>
          <p class="dark-grey-text mt-4"><i class="fas fa-quote-left pr-2"></i>sodie nunca mais! Shirlei, acho que gostamos, na verdade o melhor bolo de leite condensado com
            morango! Um bolo desse era para umas 15 pessoas mas deu tranquilo para eu, meu marido e uns 3
            amigos comer! Rs Quase não sobrou para o café amanhã, viu na foto! com replay! Muito obrigado!</p>
      </li>
    </ul>
  </div>
</div>
<br />