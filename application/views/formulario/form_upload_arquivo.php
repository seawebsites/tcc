<div class="container mt-5">
    

    <div class="row">
        <div class="card mx-auto cl-md-6 ">
            <div class="card-header mt-3">
                <h3> Selecione um arquivo para enviar ao servidor</h3>
            </div>
            <div class="card-body">
                <form id="upload-form" method="POST" enctype="multipart/form-data">
                    <div class="form-group">
                        <label for="nome">Nome</label>
                        <input type="text" class="form-control" id="nome" name="nome" placeholder="Nome do usuario">
                    </div>
                    <div class="form-group">
                        <label for="email">E-mail</label>
                        <input type="text" class="form-control" id="email" name="email" placeholder="E-mail de contato">
                    </div>
                    <div class="custom-file">
                        <input type="file" class="custom-file-input" name="arquivo" id="arquivo" lang="br">
                        <label class="custom-file-label" data-browse="Buscar" for="arquivo">Selecionar Arquivo</label>
                        <br>
                        <div class="text-center text-md-left">
                            <a class="btn btn-info " onclick="document.getElementById('upload-form').submit();">Enviar</a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<div class="container"><?= $lista_arquivos ?></div>