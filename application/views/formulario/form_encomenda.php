<br>
<div id="portfolio-heading" class="portfolio-heading section-heading">Faça a sua encomenda aqui</div>
<br>
<div class="container col-md-6">
<div>
      <h5 class="card-title pt-2"><img src="" class="animated jackInTheBox"><strong></strong></h3></h5>
      
    </div>
    <section class="text-center my-5">
<div class="wrapper-carousel-fix">
  <div id="carousel-example-1" class="carousel no-flex testimonial-carousel slide carousel-fade" data-ride="carousel"
    data-interval="false">
    <div class="carousel-inner" role="listbox">
      <div class="carousel-item active">
        <div class="testimonial">
          <h4 class="font-weight-bold">Você sonha e a gente realiza!</h4>
        </div>
      </div>
    </div>
  </div>
</div>
</section>
</div>
<div class="container">
  <div class="row">
    <div class="d-flex justify-content-md-center">
      <div class="col-4 col-md-4">
        <div class="view overlay">
          <img class="img-fluid" alt="Responsive image" width="600" height="400" src="<?= base_url('assets/mdb/img/Imagens/encomenda.jpg') ?>" />
        </div>
      </div>
    </div>
  </div>
</div>
<div class="modal-dialog cascading-modal" role="document">
  <div class="modal-content">
    <div class="modal-header  white-text" style="background-color: #20B2AA;">
      <h4 class="title"><i class="fas fa-pencil-alt"></i>Faça seu Pedido</h4>
    </div>
   
    <div class="modal-body">
      <form method="POST">
        <div class="modal-body">
          <label for="nome">Nome</label>
          <input required type="text" id="nome" name="nome" class="form-control form-control-sm" placeholder="Ex. João da Silva">
          <br>
          <label for="email">Email</label>
          <input required type="email" id="email" name="email" class="form-control form-control-sm" placeholder="exemplo@exemplo.com">
          <br>
          <label for="telefone">Telefone</label>
          <input required type="text" id="telefone" name="telefone" class="form-control form-control-sm" placeholder="11923456789">
          <br>
          <form method="GET">
            <label for="cep">CEP</label>
            <input name="cep" type="text" id="cep" class="form-control form-control-sm" placeholder="12345678" value="" onblur="pesquisacep(this.value);">
            <br>
            <label for="rua">Rua</label>
            <input required type="text" name="rua" id="rua" class="form-control form-control-sm">
            <br>
            <label for="bairro">Bairro</label>
            <input required type="text" name="bairro" id="bairro" class="form-control form-control-sm">
            <br>
            <label for="numero">Número</label>
            <input required type="text" name="numero" id="numero" class="form-control form-control-sm">
            <br>
            <label for="cidade">Cidade</label>
            <input required type="text" name="cidade" id="cidade" class="form-control form-control-sm">
            <br>
            <label for="uf">Estado</label>
            <input required type="text" name="uf" id="uf" class="form-control form-control-sm">
            <br>
          </form>
          <label for="mensagem">Faça seu pedido</label>
          <textarea required type="text" id="mensagem" name="mensagem" class="md-textarea form-control"></textarea>
          <div class="text-center mt-4 mb-2">
            <button class="btn btn-default my-4 btn-block" type="submit">Enviar</button>
          </div>
        </div>
      </form>
      <div class="modal fade" id="centralModalSuccess" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
  aria-hidden="true">
  <div class="modal-dialog modal-notify modal-success" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <p class="heading lead">Como funciona nosso sistema de fidelização</p>

        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true" class="white-text">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="text-center">
          <i class="fas fa-check fa-4x mb-3 animated rotateIn"></i>
          <p>Nosso sistema consiste em que, na compra de um dos nossos bolos grandes, nossos clientes irão sendo pontuados até atingir uma máxima de dez pontos.
        Atingida essa máxima, trocará os pontos adquiridos por um bolo grande com o sabor sa sua escolha em nossa loja.
        Para se tornar um participante basta se inscrever no formulário abaixo, preenchendo os campos e efetuando seu cadastro.
        <br />
         Gostou? Participe agora mesmo!</p>
        </div>
      </div>
      <div class="modal-footer justify-content-center">
        <a  href="<?= base_url('index.php/Fidelizacao') ?>" type="button" class="btn btn-success">Quero Participar</a>
        <a type="button" class="btn btn-outline-success waves-effect" data-dismiss="modal">Não Obrigado</a>
      </div>
    </div>
  </div>
</div>
<div class="text-center">
<p>Ainda não conhece nosso sitema de fidelização? 
  <br />
  Clique e saiba mais!</p>
            <a href="" class="btn btn-default btn-rounded mb-4" data-toggle="modal" data-target="#centralModalSuccess">Quero Conhecer</a>
          </div>
    </div>
  </div>
</div>
