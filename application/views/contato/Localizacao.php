<br>
<div id="portfolio-heading" class="portfolio-heading section-heading">Venha nos visitar!</div>
<br>

<div class="container">
  <div class="row">
    <div class="d-flex justify-content-md-between">
      <div class="col-lg-7">
        <div id="map-container-google-11" class="z-depth-1-half map-container-6">
          <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3660.5236578304457!2d-46.537915385024384!3d-23.441568384742332!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x94cec1530d666149%3A0x1820d28615180a7e!2sCake+360+Casa+de+Bolo!5e0!3m2!1spt-BR!2sbr!4v1559074893256!5m2!1spt-BR!2sbr" frameborder="0" style="border:0" allowfullscreen></iframe>
        </div>
      </div>
      <div class="col-5 text-center">
        <h3>Informações de Contato:</h3>
        <p><img src="https://img.icons8.com/dusk/50/000000/timer.png"></p>
        <h5>Horário de atendimento:</h5>
        <p>Terça à domingo das 9:00hrs às 19:00hrs</p>
        <p><img src="https://img.icons8.com/dusk/50/000000/phone-not-being-used.png"></p>
        <p>+55 11 4574-1905 ou +55 11 98858-8235</p>
        <p><img src="https://img.icons8.com/dusk/50/000000/secured-letter.png"></p>
        <p>cake360casadebolo@gmail.com</p>
      </div>
    </div>
  </div>
</div>
<br>