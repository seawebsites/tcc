<div id="portfolio-heading" class="portfolio-heading section-heading">Galeria de Fotos</div>

<br> <br>

<div class="container">
    <div class="row">
        <div class="d-flex justify-content-md-center">
            <div class="col-4 col-md-4">
                <img class="img-fluid "  alt="Responsive image" width="600" height="400" src="<?= base_url('assets/mdb/img/Imagens/bolo-branco.jpg') ?>">
            </div>
            <div class="col-4 col-md-4">
                <img class="img-fluid" alt="Responsive image" width="600" height="400" src="<?= base_url('assets/mdb/img/Imagens/bolo-branco2.jpg') ?>">
            </div>
            <div class="col-4 col-md-4">
                <img class="img-fluid" alt="Responsive image" width="600" height="400" src="<?= base_url('assets/mdb/img/Imagens/bolo-branco3.jpg') ?>">
            </div>
        </div>
        <br>
        <div class="d-flex justify-content-md-center">
            <div class="col-4 col-md-4">
                <img class="img-fluid" alt="Responsive image" width="600" height="400" src="<?= base_url('assets/mdb/img/Imagens/bolo-brigadeiro1.jpg') ?>">
            </div>
            <div class="col-4 col-md-4">
                <img class="img-fluid" alt="Responsive image" width="600" height="400" src="<?= base_url('assets/mdb/img/Imagens/bolo-brigadeiro2.jpg') ?>">
            </div>
            <div class="col-4 col-md-4">
                <img class="img-fluid" alt="Responsive image" width="600" height="400" src="<?= base_url('assets/mdb/img/Imagens/bolo-brigadeiro3.jpg') ?>">
            </div>
        </div>
        <br>
        <div class="d-flex justify-content-md-center">
            <div class="col-4 col-md-4">
                <img class="img-fluid" alt="Responsive image" width="600" height="400" src="<?= base_url('assets/mdb/img/Imagens/bolo-brigadeiroo.jpg') ?>">
            </div>
            <div class="col-4 col-md-4">
                <img class="img-fluid" alt="Responsive image" width="600" height="400" src="<?= base_url('assets/mdb/img/Imagens/bolo-castanha.jpg') ?>">
            </div>
            <div class="col-4 col-md-4">
                <img class="img-fluid" alt="Responsive image" width="600" height="400" src="<?= base_url('assets/mdb/img/Imagens/bolo-cenoura.jpg') ?>">
            </div>
        </div>
        <br>
        <div class="d-flex justify-content-md-center">
            <div class="col-4 col-md-4">
                <img class="img-fluid" alt="Responsive image" width="600" height="400" src="<?= base_url('assets/mdb/img/Imagens/bolo-churros.jpg') ?>">
            </div>
            <div class="col-4 col-md-4">
                <img class="img-fluid" alt="Responsive image" width="600" height="400" src="<?= base_url('assets/mdb/img/Imagens/bolo-coco.jpg') ?>">
            </div>
            <div class="col-4 col-md-4">
                <img class="img-fluid" alt="Responsive image" width="600" height="400" src="<?= base_url('assets/mdb/img/Imagens/bolo-felpuda.jpg') ?>">
            </div>
        </div>
        <br>
        <div class="d-flex justify-content-md-center">
            <div class="col-4 col-md-4">
                <img class="img-fluid" alt="Responsive image" width="600" height="400" src="<?= base_url('assets/mdb/img/Imagens/bolo-floresta.jpg') ?>">
            </div>
            <div class="col-4 col-md-4">
                <img class="img-fluid" alt="Responsive image" width="600" height="400" src="<?= base_url('assets/mdb/img/Imagens/bolo-fuba.jpg') ?>">
            </div>
            <div class="col-4 col-md-4">
                <img class="img-fluid" alt="Responsive image" width="600" height="400" src="<?= base_url('assets/mdb/img/Imagens/bolo-geleia.jpg') ?>">
            </div>
        </div>
        <br>
        <div class="d-flex justify-content-md-center">
            <div class="col-4 col-md-4">
                <img class="img-fluid" alt="Responsive image" width="600" height="400" src="<?= base_url('assets/mdb/img/Imagens/bolo-gotas.jpg') ?>">
            </div>
            <div class="col-4 col-md-4">
                <img class="img-fluid" alt="Responsive image" width="600" height="400" src="<?= base_url('assets/mdb/img/Imagens/bolo-gotas2.jpg') ?>">
            </div>
            <div class="col-4 col-md-4">
                <img class="img-fluid" alt="Responsive image" width="600" height="400" src="<?= base_url('assets/mdb/img/Imagens/bolo-limao.jpg') ?>">
            </div>
        </div>
        <br>
        <div class="d-flex justify-content-md-center">
            <div class="col-4 col-md-4">
                <img class="img-fluid" alt="Responsive image" width="600" height="400" src="<?= base_url('assets/mdb/img/Imagens/bolo-lol.jpg') ?>">
            </div>
            <div class="col-4 col-md-4">
                <img class="img-fluid" alt="Responsive image" width="600" height="400" src="<?= base_url('assets/mdb/img/Imagens/bolo-maracuja.jpg') ?>">
            </div>
            <div class="col-4 col-md-4">
                <img class="img-fluid" alt="Responsive image" width="600" height="400" src="<?= base_url('assets/mdb/img/Imagens/bolo-mel.jpg') ?>">
            </div>
        </div>
        <br>
        <div class="d-flex justify-content-md-center">
            <div class="col-4 col-md-4">
                <img class="img-fluid" alt="Responsive image" width="600" height="400" src="<?= base_url('assets/mdb/img/Imagens/bolo-ninho.jpg') ?>">
            </div>
            <div class="col-4 col-md-4">
                <img class="img-fluid" alt="Responsive image" width="600" height="400" src="<?= base_url('assets/mdb/img/Imagens/bolo-ninho3.jpg') ?>">
            </div>
            <div class="col-4 col-md-4">
                <img class="img-fluid" alt="Responsive image" width="600" height="400" src="<?= base_url('assets/mdb/img/Imagens/bolo-niver.jpg') ?>">
            </div>
        </div>
        <br>
        <div class="d-flex justify-content-md-center">
            <div class="col-4 col-md-4">
                <img class="img-fluid" alt="Responsive image" width="600" height="400" src="<?= base_url('assets/mdb/img/Imagens/bolo-niver2.jpg') ?>">
            </div>
            <div class="col-4 col-md-4">
                <img class="img-fluid" alt="Responsive image" width="600" height="400" src="<?= base_url('assets/mdb/img/Imagens/bolo-niver3.jpg') ?>">
            </div>
            <div class="col-4 col-md-4">
                <img class="img-fluid" alt="Responsive image" width="600" height="400" src="<?= base_url('assets/mdb/img/Imagens/bolo-niver4.jpg') ?>">
            </div>
        </div>
        <br>
        </div>
    </div>
