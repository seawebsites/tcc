<div class="modal fade" id="modalExemplo" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog " role="document">
    <div class="modal-content" id="cor">
      <div class="container">
        <div class="row">
          <div class="mt-5 mx-auto col-md-12 white-text">
            <?php echo form_open("auth/login"); ?>
            <form action="auth/login" class="text-center border-ligth p-5 ">
              <p class="h4 mb-4 text-center mt-5"><?php echo lang('login_heading'); ?></p>
              <p class="text-center"><?php echo lang('login_subheading'); ?></p>



              <input type="email" id="identity" name="identity" class="form-control mb-4" placeholder="E-mail">

              <input type="password" id="password" name="password" class="form-control mb-4" placeholder="Senha">

              <div class="d-flex justify-content-aroud">

                <div class="custom-control custom-checkbox">
                  <p class="text-center">
                    <?php echo lang('login_remember_label', 'remember'); ?>
                    <?php echo form_checkbox('remember', '1', FALSE, 'id="remember"'); ?>
                  </p></label>
                </div>
                <div class="text-center">
                  <a href="forgot_password"><?php echo lang('login_forgot_password'); ?></a>
                </div>
              </div>
              <button class="btn btn-deep-orange" type="submit">Entrar</button>
            </form>
            <p><?php echo form_close(); ?></p>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>