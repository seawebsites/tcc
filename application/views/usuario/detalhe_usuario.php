<div class="container">
    <div class="row-card">
        <div class="col-md-6 mx-auto">
            <div class="card" style="height:200px">
                <div class="row">
                <img class="card-img-top col-md-4" style="height:198px" src="https://mdbootstrap.com/img/Photos/Avatars/img%20%2810%29.jpg" alt="Card image cap">

                <div class="card-body col-md-8">
                    <h4 class="card-title"><a><?= $usuario['first_name'].' '.$usuario['last_name'] ?></a></h4>
                    <p class="card-text">E-mail: <?= $usuario['email']?></p>
                    <p class="card-text">Senha: <?= $usuario['password']?></p>
                    <p class="card-text">Telefone: <?= $usuario['phone']?></p>
                </div>
            </div>
        </div>
    </div>
</div>