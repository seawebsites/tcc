
<div class="container">
    <div class="row">
        <div class="col-md-10 mx-auto mt-4">
            <table class="table">
                <thead>
                    <tr>
                        <th scope="col">Nome</th>
                        <th scope="col">Sobrenome</th>
                        <th scope="col">E-mail</th>
                        <th scope="col">Senhas</th>
                        <th scope="col">Telefone</th>
                        <th scope="col"></th>
                    </tr>
                </thead>
                <tbody>
                    <?= $table ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
