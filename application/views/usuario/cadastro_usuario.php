<div class="container">
    <div class="row">
                    <div class="col-md-6 mx-auto mt-5">
            <form method="POST" class="text-center border border-light p-5">

            <p class="h4 mb-4">Cadastro de Usuário</p>

            <div class="form-row mb-4">
                <div class="col">
                <input type="text" value="<?= isset($usuario['first_name']) ? $usuario['first_name'] : '' ?>" id="first_name" name="first_name" class="form-control">
                </div>
                <div class="col">
                <input type="text" value="<?= isset($usuario['last_name']) ? $usuario['last_name'] : '' ?>" id="last_name" name="last_name" class="form-control" placeholder="Sobrenome">
                </div>
            </div>

            <input type="email" value="<?= isset($usuario['email']) ? $usuario['email'] : '' ?>" id="email" name="email" class="form-control mb-4">

            <input type="password" value="<?= isset($usuario['password']) ? $usuario['password']: '' ?>" id="password" name="password" class="form-control"  aria-describedby="defaultRegisterFormPasswordHelpBlock"><br/>
            
            <input type="phone" value="<?= isset($usuario['phone']) ? $usuario['phone']: '' ?>" id="phone" name="phone" class="form-control" placeholder="Telefone" aria-describedby="defaultRegisterFormPhoneHelpBlock">
            <small id="defaultRegisterFormPhoneHelpBlock" class="form-text text-muted mb-4">
              

            <div class="custom-control custom-checkbox">
                <input type="checkbox" class="custom-control-input" id="defaultRegisterFormNewsletter">
                <label class="custom-control-label" for="defaultRegisterFormNewsletter">Subscribe to our newsletter</label>
            </div>

            <button class="btn btn-info my-4 btn-block" type="submit">Enviar</button>

            </form>
                    </div>
                </div>
            </div>