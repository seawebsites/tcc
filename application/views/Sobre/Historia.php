<br />  

                
        


<br />
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="main-timeline">
                <div class="timeline">
                    <div class="timeline-content">
                    <section class="text-center my-5">
<div class="wrapper-carousel-fix">
  <div id="carousel-example-1" class="carousel no-flex testimonial-carousel slide carousel-fade" data-ride="carousel"
    data-interval="false">
    <div class="carousel-inner" role="listbox">
      <div class="carousel-item active">
        <div class="testimonial">
          <div class="avatar mx-auto mb-4">
          <img class="mt-4" src="<?= base_url('assets/mdb/img/Imagens/logo.png') ?>" height="200" alt="Responsive image" class="rounded-circle img-fluid"
              alt="First sample avatar image">
          </div>
          <h3 class="h2-responsive  text-center my-5">Quem é a Cake360 </h3>
          <p class="grey-text text-center w-responsive mx-auto mb-5">A Cake360 é uma casa de bolos localizada na Av Salgado Filho, nº 3.242 proxima ao Centro - Guarulhos - SP, é um estabelecimento especializado na venda de bolos, e cofecção de bolos.
    A propietaria é a dona Shirley Carvalho, que está no mercado de bolos a dois anos.
          <h4 class="font-weight-bold">Shirley Cristina</h4>
          <h6 class="font-weight-bold my-3">Dona da Loja Cake360</h6>
        </div>
      </div>
    </div>
  </div>
</div>

</section>

                        <div class="timeline-year">
                            <span>2017</span>
                        </div>
                        <h3 class="title">Fundação da Nossa Empresa</h3>
                        <p class="description">Abrimos nossa loja no dia 16/09/2017 no começo era uma sociedade formada com poucos funcionários, mas com o tempo fomos entrando no mercado, e se localizando
                            na  Av Salgado Filho, 3.242 - Centro - Guarulhos - SP.
                        </p>
</div>
                </div>
                <div class="timeline">
                    <div class="timeline-content">
                        <div class="timeline-year">
                            <span>2018</span>
                        </div>
                        <h3 class="title">Entramos a fundo no mercado</h3>
                        <p class="description">Com um ponto fixo para melhor atender nossos clientes, não muito diferente de antes, mas agora
                            temos uma referência, entramos a fundo no mercado satisfazendo nossos clientes e dando nosso melhor</p>
</div>
                </div>
                <div class="timeline">
                    <div class="timeline-content">
                        <div class="timeline-year">
                            <span>2019</span>
                        </div>
                        <h3 class="title">Mantendo nossas missões, visões e Valores</h3>
                        <p class="description">O tempo passou e ainda mantemos nossos valores de oferecer um serviço de qualidade, nossa missão de priorizar o bom atendimento
                            e nossa visão de melhorar para atender bem e sempre.</p>
</div>
                </div>
                
            </div>
        </div>
    </div>
</div>
<br />
<div class="container mt-3">
    <div class="row">
        <div class="d-flex justify-content-md-center">
            <div class="col-4 col-md-4 ">

            <div class="serviceBox">
                <div class="service-icon">
                <p><img src="https://img.icons8.com/dusk/80/000000/goal.png"></p>
                </div>
                <h3 class="title">Missão</h3>
                <hr>
                <p class="description">Nossa missão é priorizar o bom atendimento, com nossos produtos de qualidade oferecer o melhor a nossos clientes, e levar  a felicidade para todos.</p>
            </div>
               


            </div>
            <div class="col-4 col-md-4">
            
            <div class="serviceBox">
                <div class="service-icon">
                <p><img src="https://img.icons8.com/dusk/80/000000/visible.png"></p>
                </div>
                <h3 class="title">Visão</h3>
                <hr>
                <p class="description">Sempre melhorar perante nosso clientes, buscando atingir a satisfação plena, mantendo nossa flexibilidade seja em atender uma encomenda,
                    seja na realização de um pedido em nosso balcão, com responsabilidade e compromisso.

                </p>
            </div>

            </div>
            <div class="col-4 col-md-4">
            
            <div class="serviceBox">
                <div class="service-icon">
                <p><img src="https://img.icons8.com/dusk/80/000000/star.png"></p>
                </div>
                <h3 class="title">Valores</h3>
                <hr>
                <p class="description">Valorizamos o bom atendimento, o compromisso com cada cliente,o aprimoramento profissional, além de valorizar o cliente através de um atendimento especial e diferenciado.

</p>
            </div>


            </div>
        </div>
    </div>
</div>
<br />

