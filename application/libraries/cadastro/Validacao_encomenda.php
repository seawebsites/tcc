<?php
class Validacao_encomenda
{

    private $form_validation;

    function __construct()
    {
        $ci = & get_instance();
        $this->form_validation = $ci->form_validation;
    }

    public function getData()
    {
        $data['nome'] = $_POST['nome'];
        $data['email'] = $_POST['email'];
        $data['telefone'] = $_POST['telefone'];
        $data['mensagem'] = $_POST['mensagem'];
        $data['cep'] = $_POST['cep'];
        $data['rua'] = $_POST['rua'];
        $data['bairro'] = $_POST['bairro'];
        $data['numero'] = $_POST['numero'];
        $data['cidade'] = $_POST['cidade'];
        $data['uf'] = $_POST['uf'];

        return $data;
    }
    public function validacao()
    {
        $this->form_validation->set_rules('nome', 'nome', 'required|min_length[2]|max_length[25]');
        $this->form_validation->set_rules('email', 'email', 'required|min_length[5]|max_length[35]');
        $this->form_validation->set_rules('telefone', 'telefone', 'alpha_numeric|min_length[8]|max_length[11]');
        $this->form_validation->set_rules('mensagem', 'mensagem', 'required|min_length[2]|max_length[350]');
        $this->form_validation->set_rules('cep', 'cep', 'required|alpha_numeric|min_length[2]|max_length[8]');
        $this->form_validation->set_rules('rua', 'rua', 'required|min_length[5]|max_length[65]');
        $this->form_validation->set_rules('bairro', 'bairro', 'required|min_length[5]|max_length[40]');
        $this->form_validation->set_rules('numero', 'numero', 'required|alpha_numeric|min_length[1]|max_length[4]');
        $this->form_validation->set_rules('cidade', 'cidade', 'required|min_length[2]|max_length[50]');
        $this->form_validation->set_rules('uf', 'uf', 'required|min_length[1]|max_length[2]');
    }
}