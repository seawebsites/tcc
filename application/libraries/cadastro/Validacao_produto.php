<?php
class Validacao_produto
{

    private $form_validation;

    function __construct()
    {
        $ci = & get_instance();
        $this->form_validation = $ci->form_validation;
    }

    public function getData()
    {
        $data['nome_produto'] = $_POST['nome_produto'];
        $data['tamanho'] = $_POST['tamanho'];
        $data['preco'] = $_POST['preco'];

        return $data;
    }
    public function validate()
    {
        $this->form_validation->set_rules('nome_produto', 'nome_produto', 'required|min_length[2]|max_length[40]');
        $this->form_validation->set_rules('tamanho', 'tamanho', 'required|min_length[2]|max_length[10]');
        $this->form_validation->set_rules('preco', 'preco', 'required|alpha_numeric');
    }
}
