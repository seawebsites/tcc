<?php 

class Valida_CPF
{

    private $form_validation;

    function __construct()
    {
        $ci = & get_instance();
        $this->form_validation = $ci->form_validation;
    }

    public function valida(){
        $this->form_validation->set_rules('nome','nome','required|min_length[2]|max_length[100]');
        $this->form_validation->set_rules('email','email','required|min_length[5]|max_length[100]');
        $this->form_validation->set_rules('cpf', 'cpf', 'min_length[11]|max_length[13]');
    }
    public function getData(){
        $data ['nome'] = $_POST['nome'];
        $data ['email']= $_POST['email'];
        $data ['cpf']= $_POST['cpf'];
        
        return $data;
    }
//     function valid_cpf($cpf)
// {
//     // Verifiva se o número digitado contém todos os digitos
//     $cpf = str_pad(preg_replace('/[^0-9]/', '', $cpf), 11, '0', STR_PAD_LEFT);
 
//     // Verifica se nenhuma das sequências abaixo foi digitada, caso seja, retorna falso
//     if (strlen($cpf) != 11 ||
//         $cpf == '00000000000' ||
//         $cpf == '11111111111' ||
//         $cpf == '22222222222' ||
//         $cpf == '33333333333' ||
//         $cpf == '44444444444' ||
//         $cpf == '55555555555' ||
//         $cpf == '66666666666' ||
//         $cpf == '77777777777' ||
//         $cpf == '88888888888' ||
//         $cpf == '99999999999') {
//         return FALSE;
//     } else {
//         // Calcula os números para verificar se o CPF é verdadeiro
//         for ($t = 9; $t < 11; $t++) {
//             for ($d = 0, $c = 0; $c < $t; $c++) {
//                 $d += $cpf{$c} * (($t + 1) - $c);
//             }
 
//             $d = ((10 * $d) % 11) % 10;
//             if ($cpf{$c} != $d) {
//                 return FALSE;
//             }
//         }
//         return TRUE;
//     }
// }
 
}