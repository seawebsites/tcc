<?php
defined('BASEPATH') or exit('No direct script access allowed');

$active_group = 'default';
$query_builder = TRUE;

if (ENVIRONMENT !== 'production') {
	$username = 'gu1700987';
	$password = 'ZZ+jHkx8d8KjHeLuu0IFb8b39AVxJ0lrkUOSHmtCTXA=';
	$database = '';
} else {
	$username = 'root';
	$password = '';
	$database = 'dwa';
}


$db['default'] = array(
	'dsn'	=> '',
	'hostname' => 'localhost',
	'username' => $username,
	'password' => $password,
	'database' => $database,
	'dbdriver' => 'mysqli',
	'dbprefix' => '',
	'pconnect' => FALSE,
	'db_debug' => (ENVIRONMENT !== 'production'),
	'cache_on' => FALSE,
	'cachedir' => '',
	'char_set' => 'utf8',
	'dbcollat' => 'utf8_general_ci',
	'swap_pre' => '',
	'encrypt' => FALSE,
	'compress' => FALSE,
	'stricton' => FALSE,
	'failover' => array(),
	'save_queries' => TRUE
);
